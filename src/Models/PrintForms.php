<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.10
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Models;

use Closure;
use Scarlet\Core\Query\HttpClient;
use Scarlet\Exceptions\ScarletException;

class PrintForms extends Model
{
    protected const MARKINGS = 'markings';
    protected const REGISTRY = 'registry';
    protected const INTERNET_DOCUMENT = 'internetDocument';

    /**
     * @var string[] $methods
     * */
    protected $methods = [
        self::MARKINGS,
        self::REGISTRY,
        self::INTERNET_DOCUMENT,
    ];

    /**
     * @var string $currentMethod
     * */
    protected $currentMethod = self::MARKINGS;

    /** get link
     * @param Closure $closure
     * @return string
     * @throws ScarletException
     * */
    public function markings(Closure $closure)
    {
        $this->currentMethod = self::MARKINGS;

        $closure($this->builder);
        $this->validateRequest();
        $result = $this->builder->getResult();

        $baseLink = 'orders/printMarking85x85/';
        if (isset($result['is100']) && $result['is100']) {
            $baseLink = 'orders/printMarking100x100/';
        }

        $zebra = isset($result['zebra']) && $result['zebra'] ? 'zebra/zebra/' : '';
        $orders = join(
            '',
            array_map(function ($item) {
                return 'orders[]/' . $item . '/';
            }, $result['orders'])
        );
        $type = 'type/' . ($result['type'] ?? 'html');

        return HttpClient::$MY_LINK . $baseLink . $orders . $zebra . $type . '/apiKey/' . $this->httpClient->getToken();
    }

    /** get link
     * @param Closure $closure
     * @return string
     * @throws ScarletException
     * */
    public function registry(Closure $closure)
    {
        $this->currentMethod = self::REGISTRY;

        $closure($this->builder);
        $this->validateRequest();
        $result = $this->builder->getResult();

        $baseLink = 'scanSheet/printScanSheet/';
        $refs = join(
            '',
            array_map(function ($item) {
                return 'refs[]/' . $item . '/';
            }, $result['refs'])
        );
        $type = 'type/' . ($result['type'] ?? 'pdf');

        return HttpClient::$MY_LINK . $baseLink . $refs . $type . '/apiKey/' . $this->httpClient->getToken();
    }

    /** get link
     * @param Closure $closure
     * @return string
     * @throws ScarletException
     * */
    public function internetDocument(Closure $closure)
    {
        $this->currentMethod = self::INTERNET_DOCUMENT;

        $closure($this->builder);
        $this->validateRequest();
        $result = $this->builder->getResult();

        $baseLink = 'orders/printDocument/';
        $orders = join(
            '',
            array_map(function ($item) {
                return 'orders[]/' . $item . '/';
            }, $result['orders'])
        );
        $type = 'type/' . ($result['type'] ?? 'html');

        return HttpClient::$MY_LINK . $baseLink . $orders . $type . '/apiKey/' . $this->httpClient->getToken();
    }

    /**
     * @param mixed $data
     * @return null
     * */
    protected function resultAction($data)
    {
        return null;
    }

    /** validator request params
     * @return mixed
     * @throws ScarletException
     * */
    protected function validateRequest()
    {
        $prefix = 'Method [' . $this->currentMethod . ']:: ';

        switch ($this->currentMethod) {
            case self::MARKINGS:
            case self::INTERNET_DOCUMENT:
                if ($this->builder->has('orders') && is_array($this->builder->getResult()['orders'])) {
                    return null;
                }

                throw new ScarletException($prefix. '[orders] is required and be array.');

            case self::REGISTRY:
                if ($this->builder->has('refs') && is_array($this->builder->getResult()['refs'])) {
                    return null;
                }

                throw new ScarletException($prefix. '[refs] is required and be array.');
        }

        return null;
    }

    protected function getModelName(): string
    {
        return 'PrintForms';
    }
}
