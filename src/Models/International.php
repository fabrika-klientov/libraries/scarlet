<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 23.10.10
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Models;

use Closure;
use Scarlet\Core\Build\Builder;
use Scarlet\Core\Collection\Collection;
use Scarlet\Entities\International\CityEntity;
use Scarlet\Entities\International\CountryEntity;
use Scarlet\Models\Extended\GetList;

class International extends Model
{
    use GetList;

    protected const CITIES = 'getCities';
    protected const COUNTRIES = 'getCountries';

    /**
     * @var string[] $methods
     * */
    protected $methods = [
        self::COUNTRIES,
    ];

    /**
     * @var string $currentMethod
     * */
    protected $currentMethod = self::COUNTRIES;

    /**
     * @return $this
     * */
    public function countries()
    {
        $this->reBuilder(function (Builder $builder) {
            $builder->limit(300);
        });
        $this->currentMethod = self::COUNTRIES;

        return $this;
    }

    /**
     * @param Closure|null $closure
     * @return $this
     */
    public function cities(Closure $closure)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::CITIES;

        return $this;
    }

    /**
     * @return CountryEntity
     */
    public function getCountriesEntity()
    {
        return self::factoryEntity(CountryEntity::class);
    }

    /**
     * @return CityEntity
     */
    public function getCitiesEntity()
    {
        return self::factoryEntity(CityEntity::class);
    }

    /** helper mixed data in result
     * @param array|null $data
     * @return mixed
     * */
    protected function resultAction($data)
    {
        if (isset($data)) {
            $toCollect = $data['data'];
            switch ($this->currentMethod) {
                case self::COUNTRIES:
                case self::CITIES:
                    break;
            }

            return new Collection(array_map(function ($item) {
                return new static($this->httpClient, $item);
            }, $toCollect));
        }

        return new Collection();
    }

    /** validator request params
     * @return mixed
     * */
    protected function validateRequest()
    {
        $prefix = 'Method [' . $this->currentMethod . ']:: ';

        return null;
    }

    protected function getModelName(): string
    {
        return 'International';
    }
}
