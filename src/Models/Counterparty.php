<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.10
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Models;

use Closure;
use Scarlet\Core\Collection\Collection;
use Scarlet\Entities\Counterparty\CounterpartyAddressesEntity;
use Scarlet\Entities\Counterparty\CounterpartyContactPersonEntity;
use Scarlet\Entities\Counterparty\CounterpartyEntity;
use Scarlet\Entities\Counterparty\CounterpartyOptionsEntity;
use Scarlet\Models\Extended\DeleteModel;
use Scarlet\Models\Extended\GetList;
use Scarlet\Models\Extended\SaveModel;
use Scarlet\Models\Extended\UpdateModel;

class Counterparty extends Model
{
    use DeleteModel;
    use GetList;
    use SaveModel;
    use UpdateModel;

    protected const COUNTERPARTY_ADDRESSES = 'getCounterpartyAddresses';
    protected const COUNTERPARTY_OPTIONS = 'getCounterpartyOptions';
    protected const COUNTERPARTY_CONTACT_PERSONS = 'getCounterpartyContactPersons';
    protected const COUNTERPARTIES = 'getCounterparties';

    /**
     * @var string[] $methods
     * */
    protected $methods = [
        self::COUNTERPARTY_ADDRESSES,
        self::COUNTERPARTY_OPTIONS,
        self::COUNTERPARTY_CONTACT_PERSONS,
        self::COUNTERPARTIES,
        self::SAVE,
        self::UPDATE,
        self::DELETE,
    ];

    /**
     * @var string $currentMethod
     * */
    protected $currentMethod = self::COUNTERPARTIES;

    /**
     * @param Closure $closure
     * @return $this
     * */
    public function counterparties(Closure $closure)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::COUNTERPARTIES;

        return $this;
    }

    /**
     * @param Closure $closure
     * @return $this
     * */
    public function counterpartyAddresses(Closure $closure)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::COUNTERPARTY_ADDRESSES;

        return $this;
    }

    /**
     * @param Closure $closure
     * @return $this
     * */
    public function counterpartyOptions(Closure $closure)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::COUNTERPARTY_OPTIONS;

        return $this;
    }

    /**
     * @param Closure $closure
     * @return $this
     * */
    public function counterpartyContactPersons(Closure $closure)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::COUNTERPARTY_CONTACT_PERSONS;

        return $this;
    }

    /**
     * @return CounterpartyEntity
     */
    public function getCounterpartyEntity()
    {
        return self::factoryEntity(CounterpartyEntity::class);
    }

    /**
     * @return CounterpartyAddressesEntity
     */
    public function getCounterpartyAddressesEntity()
    {
        return self::factoryEntity(CounterpartyAddressesEntity::class);
    }

    /**
     * @return CounterpartyOptionsEntity
     */
    public function getCounterpartyOptionsEntity()
    {
        return self::factoryEntity(CounterpartyOptionsEntity::class);
    }

    /**
     * @return CounterpartyContactPersonEntity
     */
    public function getCounterpartyContactPersonEntity()
    {
        return self::factoryEntity(CounterpartyContactPersonEntity::class);
    }

    /** helper mixed data in result
     * @param array|null $data
     * @return mixed
     * */
    protected function resultAction($data)
    {
        if (isset($data)) {
            $toCollect = $data['data'];
            switch ($this->currentMethod) {
                case self::COUNTERPARTY_OPTIONS:
                case self::SAVE:
                case self::UPDATE:
                    $one = head($toCollect);
                    $this->data = array_merge($this->data, $one ?? []);
                    return $this;

                case self::DELETE:
                    $one = head($toCollect);
                    return isset($one['Ref']);
            }

            return new Collection(array_map(function ($item) {
                return new static($this->httpClient, $item);
            }, $toCollect));
        }

        return new Collection();
    }

    /** validator request params
     * @param string $method
     * @return mixed
     * */
    protected function validateRequest($method = null)
    {
        $method = $method ?? $this->currentMethod;
        $prefix = 'Method [' . $method . ']:: ';

        switch ($this->currentMethod) {
            /** method getCounterparties
             * */
            case self::COUNTERPARTIES:
                if ($this->builder->has('CounterpartyProperty')) {
                    return null;
                }

                return $prefix . '[CounterpartyProperty] is required (Sender/Recipient/ThirdPerson).';

            /** method getCounterpartyAddresses | getCounterpartyOptions
             * */
            case self::COUNTERPARTY_ADDRESSES:
            case self::COUNTERPARTY_OPTIONS:
            case self::COUNTERPARTY_CONTACT_PERSONS:
                if ($this->builder->has('Ref')) {
                    return null;
                }

                return $prefix . '[Ref] is required.';

            /** method save
             * */
            case self::SAVE:
                if (isset($this->data['CounterpartyType'], $this->data['CounterpartyProperty'])) {
                    // Создать Контрагента
                    if ($this->data['CounterpartyType'] == 'PrivatePerson') {
                        if (isset($this->data['FirstName'], $this->data['LastName'], $this->data['Phone'])) {
                            return null;
                        }

                        return $prefix . 'For [CounterpartyType=PrivatePerson] >> [FirstName], [LastName], [Phone] is required.';
                    }

                    if ($this->data['CounterpartyType'] == 'Organization') {
                        // Создать Контрагента с типом (юридическое лицо) или организацию
                        if ($this->data['CounterpartyProperty'] == 'Recipient') {
                            if (isset($this->data['FirstName'], $this->data['OwnershipForm']) || isset($this->data['EDRPOU'])) {
                                return null;
                            }

                            return $prefix . 'For [CounterpartyType=Organization][CounterpartyProperty=Recipient] >> [FirstName], [OwnershipForm] is required.';
                        }

                        // Создать Контрагента с типом третьего лица
                        if ($this->data['CounterpartyProperty'] == 'ThirdPerson') {
                            if (isset($this->data['EDRPOU'])) {
                                return null;
                            }

                            return $prefix . 'For [CounterpartyType=Organization][CounterpartyProperty=ThirdPerson] >> [EDRPOU] is required.';
                        }
                    }

                    return $prefix . '[CounterpartyType=' . $this->data['CounterpartyType'] . '] not supported. Only (PrivatePerson/Organization)';
                }

                return $prefix . '[CounterpartyType], [CounterpartyProperty] is required.';

            /** method update
             * */
            case self::UPDATE:
                $this->currentMethod = self::SAVE;
                $firstStatus = $this->validateRequest(self::UPDATE);
                $this->currentMethod = self::UPDATE;

                if (isset($firstStatus)) {
                    return $firstStatus;
                }

                if (isset($this->data['Ref'], $this->data['CityRef'])) {
                    return null;
                }

                return $prefix . '[Ref], [CityRef] is required.';

            /** method delete
             * */
            case self::DELETE:
                if (isset($this->data['Ref'])) {
                    return null;
                }

                return $prefix . '[Ref] is required.';
        }

        return null;
    }

    protected function getModelName(): string
    {
        return 'Counterparty';
    }
}
