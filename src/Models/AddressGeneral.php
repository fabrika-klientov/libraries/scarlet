<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.10
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Models;

use Closure;
use Scarlet\Core\Collection\Collection;
use Scarlet\Entities\AddressGeneral\SettlementsEntity;
use Scarlet\Entities\AddressGeneral\WarehousesEntity;
use Scarlet\Entities\AddressGeneral\WarehouseTypesEntity;
use Scarlet\Models\Extended\GetList;

class AddressGeneral extends Model
{
    use GetList;

    protected const SETTLEMENTS = 'getSettlements';
    protected const WAREHOUSES = 'getWarehouses';
    protected const WAREHOUSE_TYPES = 'getWarehouseTypes';

    /**
     * @var string[] $methods
     * */
    protected $methods = [
        self::SETTLEMENTS,
        self::WAREHOUSES,
        self::WAREHOUSE_TYPES,
    ];

    /**
     * @var string $currentMethod
     * */
    protected $currentMethod = self::SETTLEMENTS;

    /**
     * @param Closure|null $closure
     * @return $this
     */
    public function settlements(Closure $closure = null)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::SETTLEMENTS;

        return $this;
    }

    /**
     * @param Closure|null $closure
     * @return $this
     */
    public function warehouses(Closure $closure = null)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::WAREHOUSES;

        return $this;
    }

    /**
     * @return $this
     * */
    public function warehouseTypes()
    {
        $this->reBuilder();
        $this->currentMethod = self::WAREHOUSE_TYPES;

        return $this;
    }

    /**
     * @return WarehousesEntity
     */
    public function getWarehousesEntity()
    {
        return self::factoryEntity(WarehousesEntity::class);
    }

    /**
     * @return SettlementsEntity
     */
    public function getSettlementsEntity()
    {
        return self::factoryEntity(SettlementsEntity::class);
    }

    /**
     * @return WarehouseTypesEntity
     */
    public function getWarehouseTypesEntity()
    {
        return self::factoryEntity(WarehouseTypesEntity::class);
    }

    /** helper mixed data in result
     * @param array|null $data
     * @return mixed
     * */
    protected function resultAction($data)
    {
        if (isset($data)) {
            $toCollect = $data['data'];

            return new Collection(array_map(function ($item) {
                return new static($this->httpClient, $item);
            }, $toCollect));
        }

        return new Collection();
    }

    /** validator request params
     * @return mixed
     * */
    protected function validateRequest()
    {
        return null;
    }

    protected function getModelName(): string
    {
        return 'AddressGeneral';
    }
}
