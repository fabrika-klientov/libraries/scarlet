<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.10
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Models;

use Closure;
use Scarlet\Core\Collection\Collection;
use Scarlet\Entities\AdditionalServiceGeneral\AdditionalServiceGeneralEntity;
use Scarlet\Entities\AdditionalServiceGeneral\CheckPossibilityForRedirectingEntity;
use Scarlet\Models\Extended\DeleteModel;
use Scarlet\Models\Extended\GetList;
use Scarlet\Models\Extended\SaveModel;

class AdditionalServiceGeneral extends Model
{
    use DeleteModel;
    use GetList;
    use SaveModel;

    protected const CHECK_POSSIBILITY_FOR_REDIRECTING = 'checkPossibilityForRedirecting';

    /**
     * @var string[] $methods
     * */
    protected $methods = [
        self::CHECK_POSSIBILITY_FOR_REDIRECTING,
        self::SAVE,
        self::DELETE,
    ];

    /**
     * @var string $currentMethod
     * */
    protected $currentMethod = self::CHECK_POSSIBILITY_FOR_REDIRECTING;

    /**
     * @param Closure $closure
     * @return $this
     * */
    public function checkPossibilityForRedirecting(Closure $closure)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::CHECK_POSSIBILITY_FOR_REDIRECTING;

        return $this;
    }

    /**
     * @return AdditionalServiceGeneralEntity
     */
    public function getAdditionalServiceGeneralEntity()
    {
        return self::factoryEntity(AdditionalServiceGeneralEntity::class);
    }

    /**
     * @return CheckPossibilityForRedirectingEntity
     */
    public function getCheckPossibilityForRedirectingEntity()
    {
        return self::factoryEntity(CheckPossibilityForRedirectingEntity::class);
    }

    /** helper mixed data in result
     * @param array|null $data
     * @return mixed
     * */
    protected function resultAction($data)
    {
        if (isset($data)) {
            $toCollect = $data['data'];

            switch ($this->currentMethod) {
                case self::CHECK_POSSIBILITY_FOR_REDIRECTING:
                case self::SAVE:
                case self::DELETE:
                    $one = head($toCollect);
                    $this->data = array_merge($this->data, $one ?? []);
                    return $this;
            }
        }

        return new Collection();
    }

    /** validator request params
     * @return mixed
     * */
    protected function validateRequest()
    {
        $prefix = 'Method [' . $this->currentMethod . ']:: ';

        switch ($this->currentMethod) {
            /** method checkPossibilityForRedirecting
             * */
            case self::CHECK_POSSIBILITY_FOR_REDIRECTING:
                if ($this->builder->has('Number')) {
                    return null;
                }

                return $prefix . '[Number] is required.';

            /** method save
             * */
            case self::SAVE:
                if ($this->data['IntDocNumber'] &&
                    $this->data['OrderType'] &&
                    $this->data['Customer'] &&
                    $this->data['ServiceType'] &&
                    $this->data['Recipient'] &&
                    $this->data['RecipientContactName'] &&
                    $this->data['RecipientPhone'] &&
                    $this->data['PayerType'] &&
                    $this->data['PaymentMethod'] &&
                    $this->data['Note']
                ) {
                    return null;
                }

                return $prefix . '[IntDocNumber], [OrderType], [Customer], [ServiceType], [Recipient], [RecipientContactName], [RecipientPhone], [PayerType], [PaymentMethod], [Note] is required.';

            /** method delete
             * */
            case self::DELETE:
                if ($this->data['Ref']) {
                    return null;
                }

                return $prefix . '[Ref] is required.';
        }

        return null;
    }

    protected function getModelName(): string
    {
        return 'AdditionalServiceGeneral';
    }
}
