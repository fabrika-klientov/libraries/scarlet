<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.10
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Models;

use Closure;
use Scarlet\Core\Collection\Collection;
use Scarlet\Entities\AdditionalService\AdditionalServiceEntity;
use Scarlet\Entities\AdditionalService\ChangeEWOrdersListEntity;
use Scarlet\Entities\AdditionalService\CheckPossibilityChangeEWEntity;
use Scarlet\Entities\AdditionalService\CheckPossibilityCreateReturnEntity;
use Scarlet\Entities\AdditionalService\RedirectionOrdersListEntity;
use Scarlet\Entities\AdditionalService\ReturnOrdersListEntity;
use Scarlet\Entities\AdditionalService\ReturnReasonsEntity;
use Scarlet\Entities\AdditionalService\ReturnReasonsSubtypesEntity;
use Scarlet\Models\Extended\DeleteModel;
use Scarlet\Models\Extended\GetList;
use Scarlet\Models\Extended\SaveModel;

class AdditionalService extends Model
{
    use DeleteModel;
    use GetList;
    use SaveModel;

    protected const CHECK_POSSIBILITY_CREATE_RETURN = 'CheckPossibilityCreateReturn';
    protected const RETURN_REASONS = 'getReturnReasons';
    protected const RETURN_REASONS_SUBTYPES = 'getReturnReasonsSubtypes';
    protected const RETURN_ORDERS_LIST = 'getReturnOrdersList';
    protected const CHECK_POSSIBILITY_CHANGE_EW = 'CheckPossibilityChangeEW';
    protected const CHANGE_EW_ORDERS_LIST = 'getChangeEWOrdersList';
    protected const REDIRECTION_ORDERS_LIST = 'getRedirectionOrdersList';

    /**
     * @var string[] $methods
     * */
    protected $methods = [
        self::CHECK_POSSIBILITY_CREATE_RETURN,
        self::RETURN_REASONS,
        self::RETURN_REASONS_SUBTYPES,
        self::RETURN_ORDERS_LIST,
        self::CHECK_POSSIBILITY_CHANGE_EW,
        self::CHANGE_EW_ORDERS_LIST,
        self::REDIRECTION_ORDERS_LIST,
        self::SAVE,
        self::DELETE,
    ];

    /**
     * @var string $currentMethod
     * */
    protected $currentMethod = self::CHECK_POSSIBILITY_CREATE_RETURN;

    /**
     * @param Closure $closure
     * @return $this
     * */
    public function checkPossibilityCreateReturn(Closure $closure)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::CHECK_POSSIBILITY_CREATE_RETURN;

        return $this;
    }

    /**
     * @return $this
     * */
    public function returnReasons()
    {
        $this->reBuilder();
        $this->currentMethod = self::RETURN_REASONS;

        return $this;
    }

    /**
     * @param Closure|null $closure
     * @return $this
     */
    public function returnReasonsSubtypes(Closure $closure = null)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::RETURN_REASONS_SUBTYPES;

        return $this;
    }

    /**
     * @param Closure|null $closure
     * @return $this
     */
    public function returnOrdersList(Closure $closure = null)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::RETURN_ORDERS_LIST;

        return $this;
    }

    /**
     * @param Closure $closure
     * @return $this
     * */
    public function checkPossibilityChangeEW(Closure $closure)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::CHECK_POSSIBILITY_CHANGE_EW;

        return $this;
    }

    /**
     * @param Closure|null $closure
     * @return $this
     */
    public function changeEWOrdersList(Closure $closure = null)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::CHANGE_EW_ORDERS_LIST;

        return $this;
    }

    /**
     * @param Closure|null $closure
     * @return $this
     */
    public function redirectionOrdersList(Closure $closure = null)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::REDIRECTION_ORDERS_LIST;

        return $this;
    }

    /**
     * @return AdditionalServiceEntity
     */
    public function getAdditionalServiceEntity()
    {
        return self::factoryEntity(AdditionalServiceEntity::class);
    }

    /**
     * @return ReturnOrdersListEntity
     */
    public function getReturnOrdersListEntity()
    {
        return self::factoryEntity(ReturnOrdersListEntity::class);
    }

    /**
     * @return ReturnReasonsSubtypesEntity
     */
    public function getReturnReasonsSubtypesEntity()
    {
        return self::factoryEntity(ReturnReasonsSubtypesEntity::class);
    }

    /**
     * @return ReturnReasonsEntity
     */
    public function getReturnReasonsEntity()
    {
        return self::factoryEntity(ReturnReasonsEntity::class);
    }

    /**
     * @return CheckPossibilityCreateReturnEntity
     */
    public function getCheckPossibilityCreateReturnEntity()
    {
        return self::factoryEntity(CheckPossibilityCreateReturnEntity::class);
    }

    /**
     * @return RedirectionOrdersListEntity
     */
    public function getRedirectionOrdersListEntity()
    {
        return self::factoryEntity(RedirectionOrdersListEntity::class);
    }

    /**
     * @return ChangeEWOrdersListEntity
     */
    public function getChangeEWOrdersListEntity()
    {
        return self::factoryEntity(ChangeEWOrdersListEntity::class);
    }

    /**
     * @return CheckPossibilityChangeEWEntity
     */
    public function getCheckPossibilityChangeEWEntity()
    {
        return self::factoryEntity(CheckPossibilityChangeEWEntity::class);
    }

    /** helper mixed data in result
     * @param array|null $data
     * @return mixed
     * */
    protected function resultAction($data)
    {
        if (isset($data)) {
            $toCollect = $data['data'];

            switch ($this->currentMethod) {
                case self::CHECK_POSSIBILITY_CHANGE_EW:
                case self::SAVE:
                case self::DELETE:
                    $one = head($toCollect);
                    $this->data = array_merge($this->data, $one ?? []);
                    return $this;
            }

            return new Collection(array_map(function ($item) {
                return new static($this->httpClient, $item);
            }, $toCollect));
        }

        return new Collection();
    }

    /** validator request params
     * @return mixed
     * */
    protected function validateRequest()
    {
        $prefix = 'Method [' . $this->currentMethod . ']:: ';

        switch ($this->currentMethod) {
            /** method CheckPossibilityCreateReturn
             * */
            case self::CHECK_POSSIBILITY_CREATE_RETURN:
                if ($this->builder->has('Number')) {
                    return null;
                }

                return $prefix . '[Number] is required.';

            /** method CheckPossibilityChangeEW
             * */
            case self::CHECK_POSSIBILITY_CHANGE_EW:
                if ($this->builder->has('IntDocNumber')) {
                    return null;
                }

                return $prefix . '[IntDocNumber] is required.';

            /** method save
             * */
            case self::SAVE:
                if ($this->data['IntDocNumber'] && $this->data['OrderType']) {
                    return null;
                }

                return $prefix . '[IntDocNumber], [OrderType] is required.';

            /** method delete
             * */
            case self::DELETE:
                if ($this->data['Ref']) {
                    return null;
                }

                return $prefix . '[Ref] is required.';
        }

        return null;
    }

    protected function getModelName(): string
    {
        return 'AdditionalService';
    }
}
