<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.10
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Models;

use Closure;
use Scarlet\Core\Collection\Collection;
use Scarlet\Entities\ScanSheet\ScanSheetEntity;
use Scarlet\Entities\ScanSheet\ScanSheetListEntity;
use Scarlet\Exceptions\ScarletException;
use Scarlet\Models\Extended\GetList;

class ScanSheet extends Model
{
    use GetList;

    protected const SCAN_SHEET_LIST = 'getScanSheetList';
    protected const SCAN_SHEET = 'getScanSheet';
    protected const SCAN_SHEET_DOCUMENTS = 'getScanSheetDocuments';
    protected const INSERT_DOCUMENTS = 'insertDocuments';
    protected const REMOVE_DOCUMENTS = 'removeDocuments';
    protected const DELETE_SCAN_SHEET = 'deleteScanSheet';

    /**
     * @var string[] $methods
     * */
    protected $methods = [
        self::SCAN_SHEET_LIST,
        self::SCAN_SHEET,
        self::SCAN_SHEET_DOCUMENTS,
        self::INSERT_DOCUMENTS,
        self::REMOVE_DOCUMENTS,
        self::DELETE_SCAN_SHEET,
    ];

    /**
     * @var string $currentMethod
     * */
    protected $currentMethod = self::SCAN_SHEET_LIST;

    /**
     * @return $this
     * */
    public function scanSheetList()
    {
        $this->reBuilder();
        $this->currentMethod = self::SCAN_SHEET_LIST;

        return $this;
    }

    /**
     * @param Closure $closure
     * @return $this
     * */
    public function scanSheet(Closure $closure)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::SCAN_SHEET;

        return $this;
    }

    /**
     * @param Closure $closure
     * @return $this
     * */
    public function scanSheetDocuments(Closure $closure)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::SCAN_SHEET_DOCUMENTS;

        return $this;
    }

    /**
     * @param Closure|Collection $closure
     * @return $this
     * */
    public function insertDocuments($closure)
    {
        $this->reBuilder($closure instanceof Closure ? $closure : null);
        if ($closure instanceof Collection) {
            $this->builder
                ->where(
                    'DocumentRefs',
                    $closure
                        ->map(function ($item) {
                            return $item->Ref;
                        })
                        ->all()
                );
        }
        $this->currentMethod = self::INSERT_DOCUMENTS;

        return $this;
    }

    /**
     * @param Closure|Collection $closure
     * @return $this
     * */
    public function removeDocuments($closure)
    {
        $this->reBuilder($closure instanceof Closure ? $closure : null);
        if ($closure instanceof Collection) {
            $this->builder
                ->where(
                    'DocumentRefs',
                    $closure
                        ->map(function ($item) {
                            return $item->Ref;
                        })
                        ->all()
                );
        }
        $this->currentMethod = self::REMOVE_DOCUMENTS;

        return $this;
    }

    /**
     * @param Closure|Collection $closure
     * @return $this
     * */
    public function deleteScanSheet($closure)
    {
        $this->reBuilder($closure instanceof Closure ? $closure : null);
        if ($closure instanceof Collection) {
            $this->builder
                ->where(
                    'ScanSheetRefs',
                    $closure
                        ->map(function ($item) {
                            return $item->Ref;
                        })
                        ->all()
                );
        }
        $this->currentMethod = self::DELETE_SCAN_SHEET;

        return $this;
    }

    /**
     * @return ScanSheetEntity
     */
    public function getScanSheetEntity()
    {
        return self::factoryEntity(ScanSheetEntity::class);
    }

    /**
     * @return ScanSheetListEntity
     */
    public function getScanSheetListEntity()
    {
        return self::factoryEntity(ScanSheetListEntity::class);
    }

    /** alias for get -> logic type of write
     * @return mixed
     * @throws ScarletException
     * */
    public function set()
    {
        return $this->get();
    }

    /** helper mixed data in result
     * @param array|null $data
     * @return mixed
     * */
    protected function resultAction($data)
    {
        if (isset($data)) {
            $toCollect = $data['data'];

            switch ($this->currentMethod) {
                case self::SCAN_SHEET:
                case self::INSERT_DOCUMENTS:
                    $one = head($toCollect);
                    $this->data = array_merge($this->data, $one ?: []);
                    return $this;

                case self::REMOVE_DOCUMENTS:
                case self::DELETE_SCAN_SHEET:
                    $this->data = array_merge($this->data, $toCollect);
                    return $this;
            }

            return new Collection(array_map(function ($item) {
                return new static($this->httpClient, $item);
            }, $toCollect));
        }

        return new Collection();
    }

    /** validator request params
     * @return mixed
     * */
    protected function validateRequest()
    {
        $prefix = 'Method [' . $this->currentMethod . ']:: ';

        switch ($this->currentMethod) {
            /** method getScanSheet & getScanSheetDocuments
             * */
            case self::SCAN_SHEET:
            case self::SCAN_SHEET_DOCUMENTS:
                if ($this->builder->has('Ref')) {
                    return null;
                }

                return $prefix . '[Ref] is required.';

            /** method insertDocuments | removeDocuments
             * */
            case self::INSERT_DOCUMENTS:
            case self::REMOVE_DOCUMENTS:
                if ($this->builder->has('DocumentRefs')) {
                    return null;
                }

                return $prefix . '[DocumentRefs] is required.';

            /** method deleteScanSheet
             * */
            case self::DELETE_SCAN_SHEET:
                if ($this->builder->has('ScanSheetRefs')) {
                    return null;
                }

                return $prefix . '[ScanSheetRefs] is required.';
        }

        return null;
    }

    protected function getModelName(): string
    {
        return 'ScanSheet';
    }
}
