<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.10
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Models\Extended;

use Scarlet\Core\Build\Builder;
use Scarlet\Exceptions\ScarletException;

trait DeleteModel
{
    /**
     * @return bool|mixed|null
     * @throws ScarletException
     * */
    public function delete()
    {
        $this->currentMethod = self::DELETE;

        $message = $this->validateRequest();
        if (isset($message)) {
            throw new ScarletException($message);
        }

        $result = $this->httpClient->post("{$this->getModelName()}/{$this->getMethod()}", [
            'json' => $this->requestData(new Builder($this->data)),
        ]);

        return $this->resultAction($result);
    }
}
