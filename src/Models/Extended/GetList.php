<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.10
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Models\Extended;

use Scarlet\Exceptions\ScarletException;

trait GetList
{
    /**
     * @return \Scarlet\Core\Collection\Collection|mixed|null
     * @throws ScarletException
     * */
    public function get()
    {
        $message = $this->validateRequest();
        if (isset($message)) {
            throw new ScarletException($message);
        }

        $result = $this->httpClient->post("{$this->getModelName()}/{$this->getMethod()}", [
            'json' => $this->requestData(),
        ]);

        return $this->resultAction($result);
    }

    /**
     * @return string|null
     * @throws ScarletException
     * */
    public function getSrc()
    {
        $message = $this->validateRequest();
        if (isset($message)) {
            throw new ScarletException($message);
        }

        return $this->httpClient->request('POST', "{$this->getModelName()}/{$this->getMethod()}", [
            'json' => $this->requestData(),
        ], true);
    }
}
