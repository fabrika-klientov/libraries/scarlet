<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.10
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Models;

use Scarlet\Core\Collection\Collection;
use Scarlet\Entities\CommonGeneral\CommonGeneralEntity;
use Scarlet\Models\Extended\GetList;

class CommonGeneral extends Model
{
    use GetList;

    protected const MESSAGE_CODE_TEXT = 'getMessageCodeText';

    /**
     * @var string[] $methods
     * */
    protected $methods = [
        self::MESSAGE_CODE_TEXT,
    ];

    /**
     * @var string $currentMethod
     * */
    protected $currentMethod = self::MESSAGE_CODE_TEXT;

    /**
     * @return $this
     * */
    public function messageCodeText()
    {
        $this->reBuilder();
        $this->currentMethod = self::MESSAGE_CODE_TEXT;

        return $this;
    }

    /**
     * @return CommonGeneralEntity
     */
    public function getCommonGeneralEntity()
    {
        return self::factoryEntity(CommonGeneralEntity::class);
    }

    /** helper mixed data in result
     * @param array|null $data
     * @return mixed
     * */
    protected function resultAction($data)
    {
        if (isset($data)) {
            $toCollect = $data['data'];

            return new Collection(array_map(function ($item) {
                return new static($this->httpClient, $item);
            }, $toCollect));
        }

        return new Collection();
    }

    /** validator request params
     * @return mixed
     * */
    protected function validateRequest()
    {
        return null;
    }

    protected function getModelName(): string
    {
        return 'CommonGeneral';
    }
}
