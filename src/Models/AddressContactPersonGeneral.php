<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.10
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Models;

use Closure;
use Scarlet\Core\Collection\Collection;
use Scarlet\Entities\AddressContactPersonGeneral\AddressContactPersonGeneralEntity;
use Scarlet\Models\Extended\DeleteModel;
use Scarlet\Models\Extended\GetList;
use Scarlet\Models\Extended\SaveModel;

class AddressContactPersonGeneral extends Model
{
    use DeleteModel;
    use GetList;
    use SaveModel;

    protected const GET_ADDRESSES = 'getAddresses';

    /**
     * @var string[] $methods
     * */
    protected $methods = [
        self::GET_ADDRESSES,
        self::SAVE,
        self::UPDATE,
        self::DELETE,
    ];

    /**
     * @var string $currentMethod
     * */
    protected $currentMethod = self::GET_ADDRESSES;

    /**
     * @param Closure|null $closure
     * @return $this
     */
    public function addresses(Closure $closure = null)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::GET_ADDRESSES;

        return $this;
    }

    /**
     * @return AddressContactPersonGeneralEntity
     */
    public function getAddressesEntity()
    {
        return self::factoryEntity(AddressContactPersonGeneralEntity::class);
    }

    /** helper mixed data in result
     * @param array|null $data
     * @return mixed
     * */
    protected function resultAction($data)
    {
        if (isset($data)) {
            $toCollect = $data['data'];
            switch ($this->currentMethod) {
                case self::SAVE:
                    $one = head($toCollect);
                    $this->data = array_merge($this->data, $one ?? []);
                    return $this;

                case self::DELETE:
                    $one = head($toCollect);
                    return isset($one['Ref']);
            }

            return new Collection(array_map(function ($item) {
                return new static($this->httpClient, $item);
            }, $toCollect));
        }

        return new Collection();
    }

    /** validator request params
     * @return mixed
     * */
    protected function validateRequest()
    {
        $prefix = 'Method [' . $this->currentMethod . ']:: ';

        switch ($this->currentMethod) {
            /** method getAddresses
             * */
            case self::GET_ADDRESSES:
                if ($this->builder->has('ContactPersonRef')) {
                    return null;
                }

                return $prefix . '[ContactPersonRef] is required.';

            /** method save
             * */
            case self::SAVE:
                if (isset($this->data['AddressRef'], $this->data['AddressType'], $this->data['ContactPersonRef'], $this->data['SettlementRef'])) {
                    return null;
                }

                return $prefix . '[AddressRef], [AddressType], [ContactPersonRef] and [SettlementRef] are required.';

            /** method delete
             * */
            case self::DELETE:
                if (isset($this->data['Ref'], $this->data['ContactPersonRef'])) {
                    return null;
                }

                return $prefix . '[Ref] and [ContactPersonRef] are required.';
        }

        return null;
    }

    protected function getModelName(): string
    {
        return 'AddressContactPersonGeneral';
    }
}
