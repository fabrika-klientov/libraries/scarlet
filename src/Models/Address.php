<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.10
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Models;

use Closure;
use Scarlet\Core\Collection\Collection;
use Scarlet\Entities\Address\AddressEntity;
use Scarlet\Entities\Address\AreasEntity;
use Scarlet\Entities\Address\BuildingsEntity;
use Scarlet\Entities\Address\CitiesEntity;
use Scarlet\Entities\Address\SettlementsEntity;
use Scarlet\Entities\Address\SettlementStreetsEntity;
use Scarlet\Entities\Address\StreetEntity;
use Scarlet\Models\Extended\DeleteModel;
use Scarlet\Models\Extended\GetList;
use Scarlet\Models\Extended\SaveModel;
use Scarlet\Models\Extended\UpdateModel;

class Address extends Model
{
    use DeleteModel;
    use GetList;
    use SaveModel;
    use UpdateModel;

    protected const SEARCH_SETTLEMENTS = 'searchSettlements';
    protected const SEARCH_SETTLEMENT_STREETS = 'searchSettlementStreets';
    protected const AREAS = 'getAreas';
    protected const CITIES = 'getCities';
    protected const BUILDINGS = 'getBuildings';
    protected const STREET = 'getStreet';

    /**
     * @var string[] $methods
     * */
    protected $methods = [
        self::SEARCH_SETTLEMENTS,
        self::SEARCH_SETTLEMENT_STREETS,
        self::AREAS,
        self::CITIES,
        self::BUILDINGS,
        self::STREET,
        self::SAVE,
        self::UPDATE,
        self::DELETE,
    ];

    /**
     * @var string $currentMethod
     * */
    protected $currentMethod = self::AREAS;

    /**
     * @return $this
     * */
    public function areas()
    {
        $this->reBuilder();
        $this->currentMethod = self::AREAS;

        return $this;
    }

    /**
     * @param Closure|null $closure
     * @return $this
     */
    public function cities(Closure $closure = null)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::CITIES;

        return $this;
    }

    /**
     * @param Closure $closure
     * @return $this
     * */
    public function street(Closure $closure)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::STREET;

        return $this;
    }
    /**
     * @param Closure $closure
     * @return $this
     * */
    public function buildings(Closure $closure)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::BUILDINGS;

        return $this;
    }

    /**
     * @param Closure $closure
     * @return $this
     * */
    public function searchSettlements(Closure $closure)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::SEARCH_SETTLEMENTS;

        return $this;
    }

    /**
     * @param Closure $closure
     * @return $this
     * */
    public function searchSettlementStreets(Closure $closure)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::SEARCH_SETTLEMENT_STREETS;

        return $this;
    }

    /**
     * @return AddressEntity
     */
    public function getAddressEntity()
    {
        return self::factoryEntity(AddressEntity::class);
    }

    /**
     * @return SettlementsEntity
     */
    public function getSettlementsEntity()
    {
        return self::factoryEntity(SettlementsEntity::class);
    }

    /**
     * @return SettlementStreetsEntity
     */
    public function getSettlementStreetsEntity()
    {
        return self::factoryEntity(SettlementStreetsEntity::class);
    }

    /**
     * @return AreasEntity
     */
    public function getAreasEntity()
    {
        return self::factoryEntity(AreasEntity::class);
    }

    /**
     * @return CitiesEntity
     */
    public function getCitiesEntity()
    {
        return self::factoryEntity(CitiesEntity::class);
    }

    /**
     * @return StreetEntity
     */
    public function getStreetEntity()
    {
        return self::factoryEntity(StreetEntity::class);
    }

    /**
     * @return BuildingsEntity
     */
    public function getBuildingsEntity()
    {
        return self::factoryEntity(BuildingsEntity::class);
    }

    /** helper mixed data in result
     * @param array|null $data
     * @return mixed
     * */
    protected function resultAction($data)
    {
        if (isset($data)) {
            $toCollect = $data['data'];
            switch ($this->currentMethod) {
                case self::SEARCH_SETTLEMENTS:
                case self::SEARCH_SETTLEMENT_STREETS:
                    $toCollect = head($toCollect)['Addresses'] ?? [];
                    break;

                case self::SAVE:
                case self::UPDATE:
                    $one = head($toCollect);
                    $this->data = array_merge($this->data, $one ?? []);
                    return $this;

                case self::DELETE:
                    $one = head($toCollect);
                    return isset($one['Ref']);
            }

            return new Collection(array_map(function ($item) {
                return new static($this->httpClient, $item);
            }, $toCollect));
        }

        return new Collection();
    }

    /** validator request params
     * @return mixed
     * */
    protected function validateRequest()
    {
        $prefix = 'Method [' . $this->currentMethod . ']:: ';

        switch ($this->currentMethod) {
            /** method getStreet
             * */
            case self::STREET:
                if ($this->builder->has('CityRef')) {
                    return null;
                }

                return $prefix . '[CityRef] is required.';

            /** method getBuildings
             * */
            case self::BUILDINGS:
                if ($this->builder->has('StreetRef')) {
                    return null;
                }

                return $prefix . '[StreetRef] is required.';

            /** method searchSettlements
             * */
            case self::SEARCH_SETTLEMENTS:
//                if ($this->builder->has('CityName')) {
                    return null;
//                }

//                return $prefix . '[CityName] is required.';

            /** method searchSettlementStreets
             * */
            case self::SEARCH_SETTLEMENT_STREETS:
                if ($this->builder->has('StreetName') && $this->builder->has('SettlementRef') && $this->builder->has('Limit')) {
                    return null;
                }

                return $prefix . '[StreetName], [SettlementRef] and [Limit] is required.';

            /** method save
             * */
            case self::SAVE:
                if (isset($this->data['CounterpartyRef'], $this->data['StreetRef'], $this->data['BuildingNumber'])) {
                    return null;
                }

                return $prefix . '[CounterpartyRef], [StreetRef] and [BuildingNumber] is required.';

            /** method update
             * */
            case self::UPDATE:
                if (isset($this->data['Ref'], $this->data['CounterpartyRef'], $this->data['StreetRef'], $this->data['BuildingNumber'])) {
                    return null;
                }

                return $prefix . '[Ref], [CounterpartyRef], [StreetRef] and [BuildingNumber] is required.';

            /** method delete
             * */
            case self::DELETE:
                if (isset($this->data['Ref'])) {
                    return null;
                }

                return $prefix . '[Ref] is required.';
        }

        return null;
    }

    protected function getModelName(): string
    {
        return 'Address';
    }
}
