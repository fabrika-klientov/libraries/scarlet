<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.10
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Models;

use Scarlet\Entities\ContactPerson\ContactPersonEntity;
use Scarlet\Models\Extended\DeleteModel;
use Scarlet\Models\Extended\SaveModel;
use Scarlet\Models\Extended\UpdateModel;

class ContactPerson extends Model
{
    use DeleteModel;
    use SaveModel;
    use UpdateModel;

    /**
     * @var string[] $methods
     * */
    protected $methods = [
        self::SAVE,
        self::UPDATE,
        self::DELETE,
    ];

    /**
     * @var string $currentMethod
     * */
    protected $currentMethod = self::SAVE;

    /**
     * @return ContactPersonEntity
     */
    public function getContactPersonEntity()
    {
        return self::factoryEntity(ContactPersonEntity::class);
    }

    /** helper mixed data in result
     * @param array|null $data
     * @return mixed
     * */
    protected function resultAction($data)
    {
        if (isset($data)) {
            $toCollect = $data['data'];
            switch ($this->currentMethod) {
                case self::SAVE:
                case self::UPDATE:
                    $one = head($toCollect);
                    $this->data = array_merge($this->data, $one ?? []);
                    break;

                case self::DELETE:
                    $one = head($toCollect);
                    return isset($one['Ref']);
            }
        }

        return $this;
    }

    /** validator request params
     * @return mixed
     * */
    protected function validateRequest()
    {
        $prefix = 'Method [' . $this->currentMethod . ']:: ';

        switch ($this->currentMethod) {
            /** method save
             * */
            case self::SAVE:
                if (isset($this->data['CounterpartyRef'], $this->data['FirstName'], $this->data['LastName'], $this->data['Phone'])) {
                    return null;
                }

                return $prefix . '[CounterpartyRef], [FirstName], [LastName], [Phone] is required.';

            /** method update
             * */
            case self::UPDATE:
                if (isset($this->data['CounterpartyRef'], $this->data['Ref'], $this->data['FirstName'], $this->data['LastName'], $this->data['Phone'])) {
                    return null;
                }

                return $prefix . '[CounterpartyRef], [FirstName], [Ref], [LastName], [Phone] is required.';

            /** method delete
             * */
            case self::DELETE:
                if (isset($this->data['Ref'])) {
                    return null;
                }

                return $prefix . '[Ref] is required.';
        }

        return null;
    }

    protected function getModelName(): string
    {
        return 'ContactPerson';
    }
}
