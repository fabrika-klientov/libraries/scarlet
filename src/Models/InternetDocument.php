<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.10
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Models;

use Closure;
use Scarlet\Core\Collection\Collection;
use Scarlet\Entities\InternetDocument\DocumentDeliveryDateEntity;
use Scarlet\Entities\InternetDocument\DocumentPriceEntity;
use Scarlet\Entities\InternetDocument\GenerateReportEntity;
use Scarlet\Entities\InternetDocument\InternetDocumentEntity;
use Scarlet\Entities\InternetDocument\PrintFullEntity;
use Scarlet\Models\Extended\DeleteModel;
use Scarlet\Models\Extended\GetList;
use Scarlet\Models\Extended\SaveModel;
use Scarlet\Models\Extended\UpdateModel;

class InternetDocument extends Model
{
    use DeleteModel;
    use GetList;
    use SaveModel;
    use UpdateModel;

    protected const DOCUMENT_LIST = 'getDocumentList';
    protected const DOCUMENT_PRICE = 'getDocumentPrice';
    protected const DOCUMENT_DELIVERY_DATE = 'getDocumentDeliveryDate';
    protected const GENERATE_REPORT = 'generateReport';
    protected const PRINT_FULL = 'printFull';

    /**
     * @var string[] $methods
     * */
    protected $methods = [
        self::DOCUMENT_LIST,
        self::DOCUMENT_PRICE,
        self::DOCUMENT_DELIVERY_DATE,
        self::GENERATE_REPORT,
        self::PRINT_FULL,
        self::SAVE,
        self::UPDATE,
        self::DELETE,
    ];

    /**
     * @var string $currentMethod
     * */
    protected $currentMethod = self::DOCUMENT_LIST;

    /**
     * @param Closure|null $closure
     * @return $this
     */
    public function documentList(Closure $closure = null)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::DOCUMENT_LIST;

        return $this;
    }

    /**
     * @param Closure $closure
     * @return $this
     * */
    public function documentPrice(Closure $closure)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::DOCUMENT_PRICE;

        return $this;
    }

    /**
     * @param Closure $closure
     * @return $this
     * */
    public function documentDeliveryDate(Closure $closure)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::DOCUMENT_DELIVERY_DATE;

        return $this;
    }

    /**
     * @param Closure $closure
     * @return $this
     * */
    public function generateReport(Closure $closure)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::GENERATE_REPORT;

        return $this;
    }

    /**
     * @param Closure $closure
     * @return $this
     * */
    public function printFull(Closure $closure)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::PRINT_FULL;

        return $this;
    }

    /**
     * @return InternetDocumentEntity
     */
    public function getInternetDocumentEntity()
    {
        return self::factoryEntity(InternetDocumentEntity::class);
    }

    /**
     * @return DocumentPriceEntity
     */
    public function getDocumentPriceEntity()
    {
        return self::factoryEntity(DocumentPriceEntity::class);
    }

    /**
     * @return DocumentDeliveryDateEntity
     */
    public function getDocumentDeliveryDateEntity()
    {
        return self::factoryEntity(DocumentDeliveryDateEntity::class);
    }

    /**
     * @return GenerateReportEntity
     */
    public function getGenerateReportEntity()
    {
        return self::factoryEntity(GenerateReportEntity::class);
    }

    /**
     * @return PrintFullEntity
     */
    public function getPrintFullEntity()
    {
        return self::factoryEntity(PrintFullEntity::class);
    }

    /** helper mixed data in result
     * @param array|null $data
     * @return mixed
     * */
    protected function resultAction($data)
    {
        if (isset($data)) {
            $toCollect = $data['data'];
            switch ($this->currentMethod) {
                case self::SAVE:
                case self::UPDATE:
                case self::DOCUMENT_PRICE:
                case self::DOCUMENT_DELIVERY_DATE:
                    $one = head($toCollect);
                    $this->data = array_merge($this->data, $one ?? []);
                    return $this;

                case self::DELETE:
                    $one = head($toCollect);
                    return isset($one['Ref']);
            }

            return new Collection(array_map(function ($item) {
                return new static($this->httpClient, $item);
            }, $toCollect));
        }

        return new Collection();
    }

    /** validator request params
     * @param string $method
     * @return mixed
     * */
    protected function validateRequest($method = null)
    {
        $method = $method ?? $this->currentMethod;
        $prefix = 'Method [' . $method . ']:: ';

        switch ($this->currentMethod) {
            /** method getDocumentPrice
             * */
            case self::DOCUMENT_PRICE:
                if ($this->builder->has('CitySender') && $this->builder->has('CityRecipient') && $this->builder->has('Weight')) {
                    return null;
                }

                return $prefix . '[CitySender], [CityRecipient], [Weight] is required.';

            /** method getDocumentDeliveryDate
             * */
            case self::DOCUMENT_DELIVERY_DATE:
                if ($this->builder->has('CitySender') && $this->builder->has('CityRecipient') && $this->builder->has('ServiceType')) {
                    return null;
                }

                return $prefix . '[CitySender], [CityRecipient], [ServiceType] is required.';

            /** method generateReport
             * */
            case self::GENERATE_REPORT:
                if ($this->builder->has('Type') && $this->builder->has('DateTime')) {
                    return null;
                }

                return $prefix . '[Type], [DateTime] is required.';

            /** method printFull
             * */
            case self::PRINT_FULL:
                if ($this->builder->has('printForm') && $this->builder->has('Type')) {
                    return null;
                }

                return $prefix . '[printForm], [Type] is required.';

            /** method save
             * */
            case self::SAVE:
                if (isset(
                    $this->data['PayerType'],
                    $this->data['PaymentMethod'],
                    $this->data['CargoType'],
                    $this->data['ServiceType'],
                    $this->data['Cost'],
                    $this->data['CitySender'],
                    $this->data['Sender'],
                    $this->data['SenderAddress'],
                    $this->data['ContactSender'],
                    $this->data['SendersPhone'],
                    $this->data['RecipientsPhone']
                )) {
                    return null;
                }

                return $prefix . '[PayerType], [PaymentMethod], [CargoType], [ServiceType], [Cost], [CitySender], [Sender], [SenderAddress], [ContactSender], [SendersPhone], [RecipientsPhone] is required.';

            /** method update
             * */
            case self::UPDATE:
                $this->currentMethod = self::SAVE;
                $firstStatus = $this->validateRequest(self::UPDATE);
                $this->currentMethod = self::UPDATE;

                if (isset($firstStatus)) {
                    return $firstStatus;
                }

                if (isset($this->data['Ref'])) {
                    return null;
                }

                return $prefix . '[Ref] is required.';

            /** method delete
             * */
            case self::DELETE:
                if (isset($this->data['DocumentRefs'])) {
                    return null;
                }

                return $prefix . '[DocumentRefs] is required.';
        }

        return null;
    }

    protected function getModelName(): string
    {
        return 'InternetDocument';
    }
}
