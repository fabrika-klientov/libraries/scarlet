<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.10
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Models;

use Closure;
use Scarlet\Core\Collection\Collection;
use Scarlet\Entities\Common\CommonEntity;
use Scarlet\Models\Extended\GetList;

class Common extends Model
{
    use GetList;

    protected const TIME_INTERVALS = 'getTimeIntervals';
    protected const CARGO_TYPES = 'getCargoTypes';
    protected const BACKWARD_DELIVERY_CARGO_TYPES = 'getBackwardDeliveryCargoTypes';
    protected const PALLETS_LIST = 'getPalletsList';
    protected const TYPES_OF_PAYERS = 'getTypesOfPayers';
    protected const TYPES_OF_PAYERS_FOR_REDELIVERY = 'getTypesOfPayersForRedelivery';
    protected const PACK_LIST = 'getPackList';
    protected const TIRES_WHEELS_LIST = 'getTiresWheelsList';
    protected const CARGO_DESCRIPTION_LIST = 'getCargoDescriptionList';
    protected const SERVICE_TYPES = 'getServiceTypes';
    protected const TYPES_OF_COUNTERPARTIES = 'getTypesOfCounterparties';
    protected const PAYMENT_FORMS = 'getPaymentForms';
    protected const OWNERSHIP_FORMS_LIST = 'getOwnershipFormsList';

    /**
     * @var string[] $methods
     * */
    protected $methods = [
        self::TIME_INTERVALS,
        self::CARGO_TYPES,
        self::BACKWARD_DELIVERY_CARGO_TYPES,
        self::PALLETS_LIST,
        self::TYPES_OF_PAYERS,
        self::TYPES_OF_PAYERS_FOR_REDELIVERY,
        self::PACK_LIST,
        self::TIRES_WHEELS_LIST,
        self::CARGO_DESCRIPTION_LIST,
        self::SERVICE_TYPES,
        self::TYPES_OF_COUNTERPARTIES,
        self::PAYMENT_FORMS,
        self::OWNERSHIP_FORMS_LIST,
    ];

    /**
     * @var string $currentMethod
     * */
    protected $currentMethod = self::TIME_INTERVALS;

    /**
     * @param Closure $closure
     * @return $this
     * */
    public function timeIntervals(Closure $closure)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::TIME_INTERVALS;

        return $this;
    }

    /**
     * @return $this
     * */
    public function cargoTypes()
    {
        $this->reBuilder();
        $this->currentMethod = self::CARGO_TYPES;

        return $this;
    }

    /**
     * @return $this
     * */
    public function backwardDeliveryCargoTypes()
    {
        $this->reBuilder();
        $this->currentMethod = self::BACKWARD_DELIVERY_CARGO_TYPES;

        return $this;
    }

    /**
     * @return $this
     * */
    public function palletsList()
    {
        $this->reBuilder();
        $this->currentMethod = self::PALLETS_LIST;

        return $this;
    }

    /**
     * @return $this
     * */
    public function typesOfPayers()
    {
        $this->reBuilder();
        $this->currentMethod = self::TYPES_OF_PAYERS;

        return $this;
    }

    /**
     * @return $this
     * */
    public function typesOfPayersForRedelivery()
    {
        $this->reBuilder();
        $this->currentMethod = self::TYPES_OF_PAYERS_FOR_REDELIVERY;

        return $this;
    }

    /**
     * @param Closure|null $closure
     * @return $this
     */
    public function packList(Closure $closure = null)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::PACK_LIST;

        return $this;
    }

    /**
     * @return $this
     * */
    public function tiresWheelsList()
    {
        $this->reBuilder();
        $this->currentMethod = self::TIRES_WHEELS_LIST;

        return $this;
    }

    /**
     * @param Closure|null $closure
     * @return $this
     */
    public function cargoDescriptionList(Closure $closure = null)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::CARGO_DESCRIPTION_LIST;

        return $this;
    }

    /**
     * @return $this
     * */
    public function serviceTypes()
    {
        $this->reBuilder();
        $this->currentMethod = self::SERVICE_TYPES;

        return $this;
    }

    /**
     * @return $this
     * */
    public function typesOfCounterparties()
    {
        $this->reBuilder();
        $this->currentMethod = self::TYPES_OF_COUNTERPARTIES;

        return $this;
    }

    /**
     * @return $this
     * */
    public function paymentForms()
    {
        $this->reBuilder();
        $this->currentMethod = self::PAYMENT_FORMS;

        return $this;
    }

    /**
     * @return $this
     * */
    public function ownershipFormsList()
    {
        $this->reBuilder();
        $this->currentMethod = self::OWNERSHIP_FORMS_LIST;

        return $this;
    }

    /**
     * @return CommonEntity
     */
    public function getCommonEntity()
    {
        return self::factoryEntity(CommonEntity::class);
    }


    /** helper mixed data in result
     * @param array|null $data
     * @return mixed
     * */
    protected function resultAction($data)
    {
        if (isset($data)) {
            $toCollect = $data['data'];

            return new Collection(array_map(function ($item) {
                return new static($this->httpClient, $item);
            }, $toCollect));
        }

        return new Collection();
    }

    /** validator request params
     * @return mixed
     * */
    protected function validateRequest()
    {
        $prefix = 'Method [' . $this->currentMethod . ']:: ';

        switch ($this->currentMethod) {
            /** method getTimeIntervals
             * */
            case self::TIME_INTERVALS:
                if ($this->builder->has('RecipientCityRef')) {
                    return null;
                }

                return $prefix . '[RecipientCityRef] is required.';
        }

        return null;
    }

    protected function getModelName(): string
    {
        return 'Common';
    }
}
