<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.10
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Models;

use Closure;
use Scarlet\Core\Collection\Collection;
use Scarlet\Entities\TrackingDocument\TrackingDocumentEntity;
use Scarlet\Models\Extended\GetList;

class TrackingDocument extends Model
{
    use GetList;

    protected const STATUS_DOCUMENTS = 'getStatusDocuments';

    /**
     * @var string[] $methods
     * */
    protected $methods = [
        self::STATUS_DOCUMENTS,
    ];

    /**
     * @var string $currentMethod
     * */
    protected $currentMethod = self::STATUS_DOCUMENTS;

    /**
     * @param Closure $closure
     * @return $this
     * */
    public function statusDocuments(Closure $closure)
    {
        $this->reBuilder($closure);
        $this->currentMethod = self::STATUS_DOCUMENTS;

        return $this;
    }

    /**
     * @return TrackingDocumentEntity
     */
    public function getTrackingDocumentEntity()
    {
        return self::factoryEntity(TrackingDocumentEntity::class);
    }

    /** helper mixed data in result
     * @param array|null $data
     * @return mixed
     * */
    protected function resultAction($data)
    {
        if (isset($data)) {
            $toCollect = $data['data'];

            return new Collection(array_map(function ($item) {
                return new static($this->httpClient, $item);
            }, $toCollect));
        }

        return new Collection();
    }

    /** validator request params
     * @return mixed
     * */
    protected function validateRequest()
    {
        $prefix = 'Method [' . $this->currentMethod . ']:: ';

        switch ($this->currentMethod) {
            /** method getStatusDocuments
             * */
            case self::STATUS_DOCUMENTS:
                if ($this->builder->has('Documents')) {
                    return null;
                }

                return $prefix . '[Documents] is required.';
        }

        return null;
    }

    protected function getModelName(): string
    {
        return 'TrackingDocument';
    }
}
