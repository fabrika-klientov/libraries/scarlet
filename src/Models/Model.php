<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Models
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.10
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Models;

use Closure;
use Scarlet\Core\Build\Builder;
use Scarlet\Core\Classes\ActiveRecord;
use Scarlet\Core\Query\HttpClient;
use Scarlet\Exceptions\ScarletException;

abstract class Model extends ActiveRecord
{
    protected const SAVE = 'save';
    protected const UPDATE = 'update';
    protected const DELETE = 'delete';

    /**
     * @var HttpClient $httpClient
     * */
    protected $httpClient;

    /**
     * @var Builder $builder
     * */
    protected $builder;

    /**
     * @var string[] $methods
     * */
    protected $methods = [];

    /**
     * @var string $currentMethod
     * */
    protected $currentMethod;

    /**
     * @param HttpClient $client
     * @param array $data
     * @return void
     * */
    public function __construct(HttpClient $client, array $data = [])
    {
        parent::__construct($data);

        $this->httpClient = $client;
        $this->builder = new Builder();
    }


    /** NP params request
     * @param Builder|null $builder
     * @return array
     */
    protected function requestData(Builder $builder = null)
    {
        $builder = $builder ?? $this->builder;
        $options = $builder->getResult();

        if (empty($options)) { // need for NP `methodProperties` should be object (not array)
            $options = (object)$options;
        }

        return [
            "apiKey" => $this->httpClient->getToken(),
            "modelName" => $this->getModelName(),
            "calledMethod" => $this->getMethod(),
            "methodProperties" => $options,
        ];
    }

    /**
     * @return string
     * */
    abstract protected function getModelName(): string;

    /** get active method
     * @return string
     * */
    protected function getMethod(): string
    {
        return $this->currentMethod;
    }

    /** clean & init Builder
     * @param Closure|null $closure
     * @return void
     */
    protected function reBuilder(Closure $closure = null)
    {
        $this->builder->clear();
        if (isset($closure)) {
            $closure($this->builder);
        }
    }

    /** check & inject local data in builder
     * @return $this
     */
    protected function checkAndInjectBuilderLocalData()
    {
        if ($this->builder->isEmpty()) {
            $this->builder = new Builder($this->data);
        }

        return $this;
    }

    /**
     * @param string $className
     * @return mixed
     * @throws ScarletException
     */
    protected function factoryEntity(string $className)
    {
        if (!is_subclass_of($className, Model::class)) {
            throw new ScarletException('Class must extends of Model');
        }

        return new $className($this->httpClient);
    }

    /** adapter method for result
     * @param array $data
     * @return \Scarlet\Core\Collection\Collection|mixed|null
     * */
    abstract protected function resultAction($data);

    /** validating before run request
     * @return mixed
     * */
    abstract protected function validateRequest();
}
