<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.11.03
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AddressContactPersonGeneral;

use Closure;
use Scarlet\Models\AddressContactPersonGeneral;

/**
 * @property string $ContactPersonRef
 * @property int $limit
 * @property int $Page
 *
 * // for store
 * @property string $AddressRef
 * @property string $AddressType
 * @property string $SettlementRef
 * @property string $BuildingNumber
 * @property string $Flat
 * @property string $Note
 *
 * @property string $Ref // for delete
 * */
class AddressContactPersonGeneralEntity extends AddressContactPersonGeneral
{
    public const ADDRESS_TYPE_WAREHOUSE = 'Warehouse';
    public const ADDRESS_TYPE_DOORS = 'Doors';

    // actions

    /**
     * @param Closure|null $closure
     * @return \Illuminate\Support\Collection
     * */
    public function doAddresses(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->addresses($closure);
        }
        $this->currentMethod = self::GET_ADDRESSES;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new AddressResultEntity($item->jsonSerialize());
            });
    }

    /**
     * @return CreatedAddressResultEntity
     * */
    public function doSave()
    {
        return new CreatedAddressResultEntity($this->save()->jsonSerialize());
    }

    /**
     * @return bool
     * */
    public function doDelete()
    {
        return $this->delete();
    }
}
