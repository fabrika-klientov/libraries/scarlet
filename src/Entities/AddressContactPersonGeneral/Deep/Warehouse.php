<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.11.03
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AddressContactPersonGeneral\Deep;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $Ref
 * @property string $CityRef
 * @property string $CityDescription
 * @property string $AddressDescription
 * @property string $WarehouseNumber
 * @property string $TypeOfWarehouse
 * @property string $General
 * @property string $TotalMaxWeightAllowed
 * @property string $PlaceMaxWeightAllowed
 * */
class Warehouse extends BaseEntity
{

}
