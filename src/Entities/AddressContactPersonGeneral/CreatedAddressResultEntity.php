<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.11.03
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AddressContactPersonGeneral;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $Ref
 * @property string $ContactPersonRef
 * @property string $CityRef
 * @property string $CityDescription
 * @property string $AddressDescription
 * @property string $General
 *
 * // only for address type
 * @property string $SettlementRef
 * @property string $SettlementDescription
 * @property string $Type
 * @property string $RegionDescription
 * @property string $AreaDescription
 * @property string $StreetRef
 * @property string $StreetDescription
 * @property string $Description
 * @property string $BuildingNumber
 * @property string $Flat
 * @property string $Floor
 * @property string $Note
 * @property string $AddressName
 * */
class CreatedAddressResultEntity extends BaseEntity
{

}
