<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.11.03
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AddressContactPersonGeneral;

use Scarlet\Entities\AddressContactPersonGeneral\Deep\Address;
use Scarlet\Entities\AddressContactPersonGeneral\Deep\Warehouse;
use Scarlet\Entities\BaseEntity;

/**
 * @property string $ContactPersonRef
 * @property array $Addresses
 * @property array $Warehouses
 * */
class AddressResultEntity extends BaseEntity
{
    /**
     * @return Address[]
     */
    public function addresses(): array
    {
        return self::collectFrom('Addresses', Address::class);
    }

    /**
     * @return Warehouse[]
     */
    public function warehouses(): array
    {
        return self::collectFrom('Warehouses', Warehouse::class);
    }
}
