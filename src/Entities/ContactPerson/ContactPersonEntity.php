<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\ContactPerson;

use Scarlet\Models\ContactPerson;

/**
 * @property string $CounterpartyRef
 * @property string $FirstName
 * @property string $LastName
 * @property string $MiddleName
 * @property string $Phone
 *
 * @property string $Ref // for update
 * */
class ContactPersonEntity extends ContactPerson
{
    // actions

    /**
     * @return ContactPersonResultEntity
     * */
    public function doSave()
    {
        return new ContactPersonResultEntity($this->save()->jsonSerialize());
    }

    /**
     * @return ContactPersonResultEntity
     * */
    public function doUpdate()
    {
        return new ContactPersonResultEntity($this->update()->jsonSerialize());
    }

    /**
     * @return bool
     * */
    public function doDelete()
    {
        return $this->delete();
    }
}
