<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\TrackingDocument;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $Number
 * @property int $Redelivery
 * @property int $RedeliverySum
 * @property string $RedeliveryNum
 * @property string $RedeliveryPayer
 * @property string $OwnerDocumentType
 * @property string $LastCreatedOnTheBasisDocumentType
 * @property string $LastCreatedOnTheBasisPayerType
 * @property string $LastCreatedOnTheBasisDateTime
 * @property string $LastTransactionStatusGM
 * @property string $LastTransactionDateTimeGM
 * @property string $DateCreated
 * @property int $DocumentWeight
 * @property int $CheckWeight
 * @property int $DocumentCost
 * @property int $SumBeforeCheckWeight
 * @property string $PayerType
 * @property string $RecipientFullName
 * @property string $RecipientDateTime
 * @property string $ScheduledDeliveryDate
 * @property string $PaymentMethod
 * @property string $CargoDescriptionString
 * @property string $CargoType
 * @property string $CitySender
 * @property string $CityRecipient
 * @property string $WarehouseRecipient
 * @property string $WarehouseSender
 * @property string $CounterpartyType
 * @property int $AfterpaymentOnGoodsCost
 * @property string $ServiceType
 * @property string $UndeliveryReasonsSubtypeDescription
 * @property int $WarehouseRecipientNumber
 * @property string $LastCreatedOnTheBasisNumber
 * @property string $PhoneRecipient
 * @property string $RecipientFullNameEW
 * @property string $WarehouseRecipientInternetAddressRef
 * @property string $MarketplacePartnerToken
 * @property string $ClientBarcode
 * @property string $RecipientAddress
 * @property string $CounterpartyRecipientDescription
 * @property string $CounterpartySenderType
 * @property string $DateScan
 * @property string $PaymentStatus
 * @property string $PaymentStatusDate
 * @property string $AmountToPay
 * @property string $AmountPaid
 * @property string $Status
 * @property string $StatusCode
 * @property string $RefEW
 * @property string $BackwardDeliverySubTypesServices
 * @property string $BackwardDeliverySubTypesActions
 * @property string $UndeliveryReasons
 * */
class StatusDocumentsResultEntity extends BaseEntity
{

}
