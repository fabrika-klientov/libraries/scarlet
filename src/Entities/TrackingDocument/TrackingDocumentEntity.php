<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\TrackingDocument;

use Closure;
use Scarlet\Core\Helpers\WithConstruct;
use Scarlet\Models\TrackingDocument;

/**
 * @property array $Documents
 * */
class TrackingDocumentEntity extends TrackingDocument
{
    use WithConstruct;

    //setters

    /**
     * @param TrackingDocumentItemEntity[] $value
     * */
    public function documents(array $value)
    {
        return self::with('Documents', $value);
    }

    // actions

    /**
     * @param Closure|null $closure
     * @return \Illuminate\Support\Collection
     * */
    public function doStatusDocuments(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->statusDocuments($closure);
        }
        $this->currentMethod = self::STATUS_DOCUMENTS;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new StatusDocumentsResultEntity($item->jsonSerialize());
            });
    }
}
