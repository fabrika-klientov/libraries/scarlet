<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\ScanSheet;

use Closure;
use Scarlet\Core\Helpers\WithConstruct;
use Scarlet\Models\ScanSheet;

/**
 * @property string $Ref // for GetScanSheet, ScanSheetDocuments
 * @property string $CounterpartyRef // for GetScanSheet
 *
 * @property string $Page // for ScanSheetDocuments
 * @property string $Limit // for ScanSheetDocuments
 *
 * @property array $ScanSheetRefs // for DeleteScanSheet
 * */
class ScanSheetListEntity extends ScanSheet
{
    use WithConstruct;

    //setters

    /**
     * @param string[] $value
     * */
    public function scanSheetRefs(array $value)
    {
        return self::with('ScanSheetRefs', $value);
    }

    // actions

    /**
     * @return \Illuminate\Support\Collection
     * */
    public function doScanSheetList()
    {
        $this->scanSheetList();
        $this->currentMethod = self::SCAN_SHEET_LIST;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new ScanSheetListResultEntity($item->jsonSerialize());
            });
    }

    /**
     * @param Closure|null $closure
     * @return ScanSheetListDetailResultEntity
     * */
    public function doScanSheet(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->scanSheet($closure);
        }
        $this->currentMethod = self::SCAN_SHEET;

        return new ScanSheetListDetailResultEntity(
            $this
                ->checkAndInjectBuilderLocalData()
                ->get()
                ->jsonSerialize()
        );
    }

    /**
     * @param Closure|null $closure
     * @return \Illuminate\Support\Collection
     * */
    public function doScanSheetDocuments(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->scanSheetDocuments($closure);
        }
        $this->currentMethod = self::SCAN_SHEET_DOCUMENTS;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new ScanSheetDocumentResultEntity($item->jsonSerialize());
            });
    }

    /**
     * @param Closure|\Scarlet\Core\Collection\Collection|null $closure
     * @return ScanSheetResultEntity
     * */
    public function doDeleteScanSheet($closure = null)
    {
        if (isset($closure)) {
            $this->deleteScanSheet($closure);
        }
        $this->currentMethod = self::DELETE_SCAN_SHEET;

        return new ScanSheetResultEntity(
            $this
                ->checkAndInjectBuilderLocalData()
                ->set()
                ->jsonSerialize()
        );
    }
}
