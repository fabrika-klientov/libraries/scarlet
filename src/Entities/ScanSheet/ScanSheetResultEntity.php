<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\ScanSheet;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $Ref
 * @property string $Number
 * @property string $Date
 * @property string $Description
 * @property array $Errors
 * @property array $Success
 * @property array $Warnings
 * @property array $Data
 * */
class ScanSheetResultEntity extends BaseEntity
{
    /**
     * @return ScanSheetSuccessLineResultEntity[]
     */
    public function success(): array
    {
        return self::collectFrom('Success', ScanSheetSuccessLineResultEntity::class);
    }
}
