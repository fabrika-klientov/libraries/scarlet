<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\ScanSheet;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $Ref
 * @property string $Number
 * @property string $DateTime
 * @property string $Count
 * @property string $CitySenderRef
 * @property string $CitySender
 * @property string $SenderAddressRef
 * @property string $SenderAddress
 * @property string $SenderRef
 * @property string $Sender
 * */
class ScanSheetListDetailResultEntity extends BaseEntity
{

}
