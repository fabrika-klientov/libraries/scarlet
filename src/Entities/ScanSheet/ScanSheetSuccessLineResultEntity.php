<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.11.25
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\ScanSheet;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $Ref
 * @property string $Number
 * */
class ScanSheetSuccessLineResultEntity extends BaseEntity
{

}
