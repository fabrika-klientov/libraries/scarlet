<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\ScanSheet;

use Closure;
use Scarlet\Core\Helpers\WithConstruct;
use Scarlet\Models\ScanSheet;

/**
 * @property string $Ref // for adding (optional) or removing
 * @property string $Date // for adding (optional)
 * @property array $DocumentRefs
 * */
class ScanSheetEntity extends ScanSheet
{
    use WithConstruct;

    //setters

    /**
     * @param string[] $value
     * */
    public function documentRefs(array $value)
    {
        return self::with('DocumentRefs', $value);
    }

    // actions

    /**
     * @param Closure|\Scarlet\Core\Collection\Collection|null $closure
     * @return ScanSheetResultEntity
     * */
    public function doInsertDocuments($closure = null)
    {
        if (isset($closure)) {
            $this->insertDocuments($closure);
        }
        $this->currentMethod = self::INSERT_DOCUMENTS;

        return new ScanSheetResultEntity(
            $this
                ->checkAndInjectBuilderLocalData()
                ->set()
                ->jsonSerialize()
        );
    }

    /**
     * @param Closure|\Scarlet\Core\Collection\Collection|null $closure
     * @return ScanSheetResultEntity
     * */
    public function doRemoveDocuments($closure = null)
    {
        if (isset($closure)) {
            $this->removeDocuments($closure);
        }
        $this->currentMethod = self::REMOVE_DOCUMENTS;

        return new ScanSheetResultEntity(
            $this
                ->checkAndInjectBuilderLocalData()
                ->set()
                ->jsonSerialize()
        );
    }
}
