<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AdditionalService;

use Scarlet\Entities\BaseEntity;

/**
 * @property bool $NonCash
 * @property string $City
 * @property string $Counterparty
 * @property string $ContactPerson
 * @property string $Address
 * @property string $Phone
 * @property string $Ref
 * */
class CheckPossibilityCreateReturnResultEntity extends BaseEntity
{

}
