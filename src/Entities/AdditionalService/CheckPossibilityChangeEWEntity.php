<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AdditionalService;

use Closure;
use Scarlet\Models\AdditionalService;

/**
 * @property int $IntDocNumber
 * */
class CheckPossibilityChangeEWEntity extends AdditionalService
{
    // actions

    /**
     * @param Closure|null $closure
     * @return CheckPossibilityChangeEWResultEntity
     * */
    public function doCheckPossibilityChangeEW(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->checkPossibilityChangeEW($closure);
        }
        $this->currentMethod = self::CHECK_POSSIBILITY_CHANGE_EW;

        return new CheckPossibilityChangeEWResultEntity(
            $this
                ->checkAndInjectBuilderLocalData()
                ->get()
                ->jsonSerialize()
        );
    }
}
