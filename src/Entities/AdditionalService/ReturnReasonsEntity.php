<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AdditionalService;

use Scarlet\Models\AdditionalService;

/**
 * */
class ReturnReasonsEntity extends AdditionalService
{
    // actions

    /**
     * @return \Illuminate\Support\Collection
     * */
    public function doReturnReasons()
    {
        $this->returnReasons();
        $this->currentMethod = self::RETURN_REASONS;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new ReturnReasonsResultEntity($item->jsonSerialize());
            });
    }
}
