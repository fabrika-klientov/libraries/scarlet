<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AdditionalService;

use Closure;
use Scarlet\Models\AdditionalService;

/**
 * @property int $Number
 * */
class CheckPossibilityCreateReturnEntity extends AdditionalService
{
    // actions

    /**
     * @param Closure|null $closure
     * @return \Illuminate\Support\Collection
     * */
    public function doCheckPossibilityCreateReturn(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->checkPossibilityCreateReturn($closure);
        }
        $this->currentMethod = self::CHECK_POSSIBILITY_CREATE_RETURN;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new CheckPossibilityCreateReturnResultEntity($item->jsonSerialize());
            });
    }
}
