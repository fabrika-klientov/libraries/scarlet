<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AdditionalService;

use Closure;
use Scarlet\Models\AdditionalService;

/**
 * @property string $Number
 * @property string $Ref
 * @property string $BeginDate
 * @property string $EndDate
 * @property string $Page
 * @property string $Limit
 * */
class ReturnOrdersListEntity extends AdditionalService
{
    // actions

    /**
     * @param Closure|null $closure
     * @return \Illuminate\Support\Collection
     * */
    public function doReturnOrdersList(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->returnOrdersList($closure);
        }
        $this->currentMethod = self::RETURN_ORDERS_LIST;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new ReturnOrdersListResultEntity($item->jsonSerialize());
            });
    }
}
