<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AdditionalService;

use Scarlet\Models\AdditionalService;

/**
 * @property string $IntDocNumber // for ChangeEWOrdersList & ReturnOrdersList
 * @property string $PaymentMethod // for ChangeEWOrdersList & ReturnOrdersList
 * @property string $OrderType // for ChangeEWOrdersList & ReturnOrdersList
 *
 * @property string $Reason // for ReturnOrdersList
 * @property string $SubtypeReason // for ReturnOrdersList
 * @property string $Note // for ReturnOrdersList
 * @property string $ReturnAddressRef // for ReturnOrdersList
 *
 * @property string $Ref // for delete
 *
 * @property string $SenderContactName // for ChangeEWOrdersList
 * @property string $SenderPhone // for ChangeEWOrdersList
 * @property string $Recipient // for ChangeEWOrdersList
 * @property string $RecipientContactName // for ChangeEWOrdersList
 * @property string $RecipientPhone // for ChangeEWOrdersList
 * @property string $PayerType // for ChangeEWOrdersList
 * @property array $BackwardDeliveryData // for ChangeEWOrdersList
 * */
class AdditionalServiceEntity extends AdditionalService
{
    // actions

    /**
     * @return AdditionalServiceResultEntity
     * */
    public function doCreateReturnOrder()
    {
        return self::doSave();
    }

    /**
     * @return AdditionalServiceResultEntity
     * */
    public function doRemoveReturnOrder()
    {
        return self::doDelete();
    }

    /**
     * @return AdditionalServiceResultEntity
     * */
    public function doCreateChangeEWOrder()
    {
        return self::doSave();
    }

    /**
     * @return AdditionalServiceResultEntity
     * */
    public function doRemoveChangeEWOrder()
    {
        return self::doDelete();
    }

    /**
     * @return AdditionalServiceResultEntity
     * */
    public function doSave()
    {
        return new AdditionalServiceResultEntity($this->save()->jsonSerialize());
    }

    /**
     * @return AdditionalServiceResultEntity
     * */
    public function doDelete()
    {
        return new AdditionalServiceResultEntity($this->delete()->jsonSerialize());
    }
}
