<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AdditionalService;

use Closure;
use Scarlet\Models\AdditionalService;

/**
 * @property string $ReasonRef
 * */
class ReturnReasonsSubtypesEntity extends AdditionalService
{
    // actions

    /**
     * @param Closure|null $closure
     * @return \Illuminate\Support\Collection
     * */
    public function doReturnReasonsSubtypes(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->returnReasonsSubtypes($closure);
        }
        $this->currentMethod = self::RETURN_REASONS_SUBTYPES;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new ReturnReasonsSubtypesResultEntity($item->jsonSerialize());
            });
    }
}
