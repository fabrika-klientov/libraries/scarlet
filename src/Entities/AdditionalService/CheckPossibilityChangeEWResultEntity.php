<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AdditionalService;

use Scarlet\Entities\BaseEntity;

/**
 * @property bool $CanChangeSender
 * @property bool $CanChangeRecipient
 * @property bool $CanChangePayerTypeOrPaymentMethod
 * @property string $SenderPhone
 * @property string $ContactPersonSender
 * @property string $RecipientPhone
 * @property string $ContactPersonRecipient
 * @property string $PayerType
 * @property string $PaymentMethod
 * @property string $SenderCounterparty
 * @property string $RecipientCounterparty
 * */
class CheckPossibilityChangeEWResultEntity extends BaseEntity
{

}
