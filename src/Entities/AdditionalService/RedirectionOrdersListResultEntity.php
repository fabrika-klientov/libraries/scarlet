<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AdditionalService;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $OrderRef
 * @property string $OrderNumber
 * @property string $DateTime
 * @property string $OrderStatus
 * @property string $DocumentNumber
 * @property string $Note
 * @property string $CityRecipient
 * @property string $RecipientAddress
 * @property string $CounterpartyRecipient
 * @property string $RecipientName
 * @property string $PhoneRecipient
 * @property string $PayerType
 * @property string $PaymentMethod
 * @property string $DeliveryCost
 * @property string $EstimatedDeliveryDate
 * @property string $ExpressWaybillNumber
 * @property string $ExpressWaybillStatus
 * */
class RedirectionOrdersListResultEntity extends BaseEntity
{

}
