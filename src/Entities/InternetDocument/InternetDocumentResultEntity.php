<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\InternetDocument;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $Ref
 * @property string $DateTime
 * @property string $PreferredDeliveryDate
 * @property string $Weight
 * @property string $SeatsAmount
 * @property string $IntDocNumber
 * @property string $Cost
 * @property string $CitySender
 * @property string $CityRecipient
 * @property string $State
 * @property string $SenderAddress
 * @property string $RecipientAddress
 * @property string $CostOnSite
 * @property string $PayerType
 * @property string $PaymentMethod
 * @property string $AfterpaymentOnGoodsCost
 * @property string $PackingNumber
 * @property string $Number
 * @property string $Posted
 * @property string $DeletionMark
 * @property string $CargoType
 * @property string $Route
 * @property string $EWNumber
 * @property string $Description
 * @property string $SaturdayDelivery
 * @property string $ExpressWaybill
 * @property string $CarCall
 * @property string $ServiceType
 * @property string $DeliveryDateFrom
 * @property string $Vip
 * @property string $AdditionalInformation
 * @property string $LastModificationDate
 * @property string $ReceiptDate
 * @property string $LoyaltyCard
 * @property string $Sender
 * @property string $ContactSender
 * @property string $SendersPhone
 * @property string $Recipient
 * @property string $ContactRecipient
 * @property string $RecipientsPhone
 * @property string $Redelivery
 * @property string $SaturdayDeliveryString
 * @property string $Note
 * @property string $ThirdPerson
 * @property string $Forwarding
 * @property string $NumberOfFloorsLifting
 * @property string $StatementOfAcceptanceTransferCargoID
 * @property string $StateId
 * @property string $StateName
 * @property string $RecipientFullName
 * @property string $RecipientPost
 * @property string $RecipientDateTime
 * @property string $RejectionReason
 * @property string $CitySenderDescription
 * @property string $CityRecipientDescription
 * @property string $SenderDescription
 * @property string $SenderContactPerson
 * @property string $RecipientDescription
 * @property string $RecipientContactPhone
 * @property string $RecipientContactPerson
 * @property string $SenderAddressDescription
 * @property string $RecipientAddressDescription
 * @property string $Printed
 * @property string $Fulfillment
 * @property string $EstimatedDeliveryDate
 * @property string $DateLastUpdatedStatus
 * @property string $CreateTime
 * @property string $ScanSheetNumber
 * @property string $InfoRegClientBarcodes
 * @property string $StatePayId
 * @property string $StatePayName
 * @property string $BackwardDeliveryCargoType
 * */
class InternetDocumentResultEntity extends BaseEntity
{

}
