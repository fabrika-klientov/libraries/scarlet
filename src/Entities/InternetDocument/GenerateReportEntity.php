<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\InternetDocument;

use Closure;
use Scarlet\Models\InternetDocument;

/**
 * @property string $DateTime
 * @property array $DocumentRefs
 * @property string $Type
 * */
class GenerateReportEntity extends InternetDocument
{
    /**
     * @param Closure|null $closure
     * @return \Illuminate\Support\Collection
     * */
    public function doGenerateReport(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->generateReport($closure);
        }
        $this->currentMethod = self::GENERATE_REPORT;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new GenerateReportResultEntity($item->jsonSerialize());
            });
    }
}
