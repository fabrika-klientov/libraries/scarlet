<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\InternetDocument;

use Closure;
use Scarlet\Core\Helpers\WithConstruct;
use Scarlet\Models\InternetDocument;

/**
 * @property string $PayerType
 * @property string $PaymentMethod
 * @property string $ThirdPerson
 * @property string $EDRPOU
 * @property string $DateTime
 * @property string $CargoType
 * @property string $VolumeGeneral
 * @property string $Weight
 * @property string $ServiceType
 * @property string $SeatsAmount
 * @property string $Description
 * @property string $Cost
 * @property string $CitySender
 * @property string $Sender
 * @property string $SenderAddress
 * @property string $ContactSender
 * @property string $SendersPhone
 * @property string $CityRecipient
 * @property string $Recipient
 * @property string $OwnershipForm
 * @property string $RecipientAddress
 * @property string $ContactRecipient
 * @property string $RecipientContactName
 * @property string $RecipientsPhone
 *
 * @property string $NewAddress
 * @property string $RecipientCityName
 * @property string $RecipientArea
 * @property string $RecipientAreaRegions
 * @property string $RecipientAddressName
 * @property string $RecipientHouse
 * @property string $RecipientFlat
 * @property string $RecipientName
 * @property string $RecipientType
 * @property string $Note
 *
 * @property string $Ref // for update
 *
 * @property string $IsTakeAttorney
 * @property string $SaturdayDelivery
 * @property string $AfterpaymentOnGoodsCost
 * @property string $LocalExpress
 * @property string $TimeInterval
 * @property string $PreferredDeliveryDate
 * @property string $PackingNumber
 * @property string $InfoRegClientBarcodes
 * @property string $AccompanyingDocuments
 * @property string $AdditionalInformation
 * @property string $NumberOfFloorsLifting
 * @property string $Elevator
 * @property string $DeliveryByHand
 * @property array $DeliveryByHandRecipients
 * @property string $ForwardingCount
 * @property string $RedBoxBarcode
 * @property string $AviaDelivery
 * @property string $specialCargo
 *
 * @property array $CargoDetails
 *
 * @property array $OptionsSeat
 * @property array $BackwardDeliveryData
 *
 * @property string $IntDocNumber // for load TTN
 * */
class InternetDocumentEntity extends InternetDocument
{
    use WithConstruct;

    // setters

    /**
     * @param InternetDocumentOptionsSeatEntity[] $value
     * */
    public function optionsSeat(array $value)
    {
        return self::with('OptionsSeat', $value);
    }

    /**
     * @param InternetDocumentBackwardDeliveryDataEntity[] $value
     * */
    public function backwardDeliveryData(array $value)
    {
        return self::with('BackwardDeliveryData', $value);
    }

    /**
     * @param InternetDocumentCargoDetailsEntity[] $value
     * */
    public function cargoDetails(array $value)
    {
        return self::with('CargoDetails', $value);
    }

    // actions

    /**
     * @param Closure|null $closure
     * @return \Illuminate\Support\Collection
     * */
    public function doDocumentList(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->documentList($closure);
        }
        $this->currentMethod = self::DOCUMENT_LIST;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new InternetDocumentResultEntity($item->jsonSerialize());
            });
    }

    /**
     * @return InternetDocumentSimpleResultEntity
     * */
    public function doSave()
    {
        return new InternetDocumentSimpleResultEntity($this->save()->jsonSerialize());
    }

    /**
     * @return InternetDocumentSimpleResultEntity
     * */
    public function doUpdate()
    {
        return new InternetDocumentSimpleResultEntity($this->update()->jsonSerialize());
    }

    /**
     * @return bool
     * */
    public function doDelete()
    {
        return $this->delete();
    }
}
