<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\InternetDocument;

use Closure;
use Scarlet\Core\Helpers\WithConstruct;
use Scarlet\Models\InternetDocument;

/**
 * @property string $CitySender
 * @property string $CityRecipient
 * @property string $Weight
 * @property string $ServiceType
 * @property string $Cost
 * @property string $CargoType
 * @property string $SeatsAmount
 * @property string $Amount
 * @property string $PackRef
 * @property array $RedeliveryCalculate
 * @property array $OptionsSeat
 * @property array $CargoDetails
 * */
class DocumentPriceEntity extends InternetDocument
{
    use WithConstruct;

    /**
     * @param DocumentPriceOptionsSeatEntity[] $value
     * */
    public function optionsSeat(array $value)
    {
        return self::with('OptionsSeat', $value);
    }

    /**
     * @param DocumentPriceRedeliveryCalculateEntity $value
     * */
    public function redeliveryCalculate(DocumentPriceRedeliveryCalculateEntity $value)
    {
        return self::with('RedeliveryCalculate', $value);
    }

    /**
     * @param DocumentPriceCargoDetailsEntity[] $value
     * */
    public function cargoDetails(array $value)
    {
        return self::with('CargoDetails', $value);
    }

    // actions

    /**
     * @param Closure|null $closure
     * @return DocumentPriceResultEntity
     */
    public function doDocumentPrice(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->documentPrice($closure);
        }
        $this->currentMethod = self::DOCUMENT_PRICE;

        return new DocumentPriceResultEntity($this->checkAndInjectBuilderLocalData()->get()->jsonSerialize());
    }
}
