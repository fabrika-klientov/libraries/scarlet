<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\InternetDocument;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $Ref
 * @property int $CostOnSite
 * @property string $EstimatedDeliveryDate
 * @property string $IntDocNumber
 * @property string $TypeDocument
 * */
class InternetDocumentSimpleResultEntity extends BaseEntity
{

}
