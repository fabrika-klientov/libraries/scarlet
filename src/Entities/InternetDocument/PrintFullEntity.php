<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.11.17
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\InternetDocument;

use Closure;
use Scarlet\Models\InternetDocument;

/**
 * @property string $printForm
 * @property array $Type
 * @property string $Position
 * @property string $Copies
 * @property string $PageFormat
 * @property string $PrintOrientation
 * @property string[] $DocumentRefs
 * @property string[] $ScanSheetRefs
 * */
class PrintFullEntity extends InternetDocument
{
    public const PRINT_FORM_MARKING_100_100 = 'Marking_100x100';
    public const PRINT_FORM_MARKING_85_85 = 'Marking_85x85';
    public const PRINT_FORM_DOCUMENT_NEW = 'Document_new';
    public const PRINT_FORM_SCAN_SHEET = 'ScanSheet';

    public const PAGE_FORMAT_A4 = 'A4';
    public const PAGE_FORMAT_A5 = 'A5';

    /**
     * @param Closure|null $closure
     * @return string|null
     * */
    public function doPrintFull(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->printFull($closure);
        }
        $this->currentMethod = self::PRINT_FULL;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->getSrc();
    }
}
