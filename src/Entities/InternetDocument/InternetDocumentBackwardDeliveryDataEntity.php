<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\InternetDocument;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $PayerType
 * @property string $CargoType
 * @property array $Services
 * @property string $RedeliveryString
 * */
class InternetDocumentBackwardDeliveryDataEntity extends BaseEntity
{
    /**
     * @return InternetDocumentBackwardDeliveryDataServicesEntity|null
     */
    public function services(): ?InternetDocumentBackwardDeliveryDataServicesEntity
    {
        return empty($this->Services) ? null : new InternetDocumentBackwardDeliveryDataServicesEntity($this->Services);
    }
}
