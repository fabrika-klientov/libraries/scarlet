<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\InternetDocument;

use Closure;
use Scarlet\Models\InternetDocument;

/**
 * @property string $DateTime
 * @property string $ServiceType
 * @property string $CitySender
 * @property string $CityRecipient
 * */
class DocumentDeliveryDateEntity extends InternetDocument
{
    /**
     * @param Closure|null $closure
     * @return DocumentDeliveryDateResultEntity
     * */
    public function doDocumentDeliveryDate(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->documentDeliveryDate($closure);
        }
        $this->currentMethod = self::DOCUMENT_DELIVERY_DATE;

        return new DocumentDeliveryDateResultEntity($this->checkAndInjectBuilderLocalData()->get()->jsonSerialize());
    }
}
