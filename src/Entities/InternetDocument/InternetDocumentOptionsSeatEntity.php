<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\InternetDocument;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $volumetricVolume
 * @property string $volumetricWidth
 * @property string $volumetricLength
 * @property string $volumetricHeight
 * @property string $weight
 * @property string $cost
 * @property string $description
 * @property string $specialCargo
 * @property string $packRef
 * */
class InternetDocumentOptionsSeatEntity extends BaseEntity
{

}
