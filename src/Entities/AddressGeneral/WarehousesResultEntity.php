<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AddressGeneral;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $SiteKey
 * @property string $Description
 * @property string $DescriptionRu
 * @property string $ShortAddress
 * @property string $ShortAddressRu
 * @property string $Phone
 * @property string $TypeOfWarehouse
 * @property string $Ref
 * @property string $Number
 * @property string $CityRef
 * @property string $CityDescription
 * @property string $CityDescriptionRu
 * @property string $SettlementRef
 * @property string $SettlementDescription
 * @property string $SettlementAreaDescription
 * @property string $SettlementRegionsDescription
 * @property string $SettlementTypeDescription
 * @property string $SettlementTypeDescriptionRu
 * @property string $Longitude
 * @property string $Latitude
 * @property string $PostFinance
 * @property string $BicycleParking
 * @property string $PaymentAccess
 * @property string $POSTerminal
 * @property string $InternationalShipping
 * @property string $SelfServiceWorkplacesCount
 * @property string $TotalMaxWeightAllowed
 * @property string $PlaceMaxWeightAllowed
 * @property array $SendingLimitationsOnDimensions
 * @property array $ReceivingLimitationsOnDimensions
 * @property array $Reception
 * @property array $Delivery
 * @property array $Schedule
 * @property string $DistrictCode
 * @property string $WarehouseStatus
 * @property string $WarehouseStatusDate
 * @property string $CategoryOfWarehouse
 * @property string $Direct
 * @property string $RegionCity
 * @property string $WarehouseForAgent
 * @property string $MaxDeclaredCost
 * */
class WarehousesResultEntity extends BaseEntity
{
    /**
     * @return WarehousesSendingLimitationsOnDimensionsResultEntity|null
     * */
    public function sendingLimitationsOnDimensions()
    {
        return empty($this->SendingLimitationsOnDimensions)
            ? null : new WarehousesSendingLimitationsOnDimensionsResultEntity($this->SendingLimitationsOnDimensions);
    }

    /**
     * @return WarehousesReceivingLimitationsOnDimensionsResultEntity|null
     * */
    public function receivingLimitationsOnDimensions()
    {
        return empty($this->ReceivingLimitationsOnDimensions)
            ? null : new WarehousesReceivingLimitationsOnDimensionsResultEntity($this->ReceivingLimitationsOnDimensions);
    }

    /**
     * @return WarehousesReceptionResultEntity|null
     * */
    public function reception()
    {
        return empty($this->Reception) ? null : new WarehousesReceptionResultEntity($this->Reception);
    }

    /**
     * @return WarehousesDeliveryResultEntity|null
     * */
    public function delivery()
    {
        return empty($this->Delivery) ? null : new WarehousesDeliveryResultEntity($this->Delivery);
    }

    /**
     * @return WarehousesScheduleResultEntity|null
     * */
    public function schedule()
    {
        return empty($this->Schedule) ? null : new WarehousesScheduleResultEntity($this->Schedule);
    }
}
