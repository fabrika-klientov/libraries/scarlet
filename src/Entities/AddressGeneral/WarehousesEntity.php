<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AddressGeneral;

use Closure;
use Scarlet\Models\AddressGeneral;

/**
 * @property string $CityName
 * @property string $CityRef
 * @property string $Page
 * @property string $Limit
 * @property string $Language
 * @property string $BicycleParking
 * @property string $TypeOfWarehouseRef
 * @property string $PostFinance
 * @property string $FindByString
 * @property string $Ref
 * @property string $SettlementRef
 * */
class WarehousesEntity extends AddressGeneral
{
    // actions

    /**
     * @param Closure|null $closure
     * @return \Illuminate\Support\Collection
     * */
    public function doWarehouses(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->warehouses($closure);
        }
        $this->currentMethod = self::WAREHOUSES;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new WarehousesResultEntity($item->jsonSerialize());
            });
    }
}
