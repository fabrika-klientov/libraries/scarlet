<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AddressGeneral;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $Ref
 * @property string $SettlementType
 * @property string $Latitude
 * @property string $Longitude
 * @property string $Description
 * @property string $DescriptionRu
 * @property string $SettlementTypeDescription
 * @property string $SettlementTypeDescriptionRu
 * @property string $Region
 * @property string $RegionsDescription
 * @property string $RegionsDescriptionRu
 * @property string $Area
 * @property string $AreaDescription
 * @property string $AreaDescriptionRu
 * @property string $Index1
 * @property string $Index2
 * @property string $IndexCOATSU1
 * @property string $Delivery1
 * @property string $Delivery2
 * @property string $Delivery3
 * @property string $Delivery4
 * @property string $Delivery5
 * @property string $Delivery6
 * @property string $Delivery7
 * @property string $Warehouse
 * @property array $Conglomerates
 * */
class SettlementsResultEntity extends BaseEntity
{

}
