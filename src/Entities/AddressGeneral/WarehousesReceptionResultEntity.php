<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AddressGeneral;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $Monday
 * @property string $Tuesday
 * @property string $Wednesday
 * @property string $Thursday
 * @property string $Friday
 * @property string $Saturday
 * @property string $Sunday
 * */
class WarehousesReceptionResultEntity extends BaseEntity
{

}
