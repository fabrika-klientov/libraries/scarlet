<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AddressGeneral;

use Scarlet\Models\AddressGeneral;

/**
 * */
class WarehouseTypesEntity extends AddressGeneral
{
    // actions

    /**
     * @return \Illuminate\Support\Collection
     * */
    public function doWarehouseTypes()
    {
        $this->warehouseTypes();
        $this->currentMethod = self::WAREHOUSE_TYPES;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new WarehouseTypesResultEntity($item->jsonSerialize());
            });
    }
}
