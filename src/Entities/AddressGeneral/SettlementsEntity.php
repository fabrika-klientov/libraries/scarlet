<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AddressGeneral;

use Closure;
use Scarlet\Models\AddressGeneral;

/**
 * @property string $AreaRef
 * @property string $Ref
 * @property string $RegionRef
 * @property string $Page
 * @property string $FindByString
 * @property string $Warehouse
 * */
class SettlementsEntity extends AddressGeneral
{
    // actions

    /**
     * @param Closure|null $closure
     * @return \Illuminate\Support\Collection
     * */
    public function doSettlements(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->settlements($closure);
        }
        $this->currentMethod = self::SETTLEMENTS;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new SettlementsResultEntity($item->jsonSerialize());
            });
    }
}
