<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AddressGeneral;

use Scarlet\Entities\BaseEntity;

/**
 * @property int $Width
 * @property int $Height
 * @property int $Length
 * */
class WarehousesSendingLimitationsOnDimensionsResultEntity extends BaseEntity
{

}
