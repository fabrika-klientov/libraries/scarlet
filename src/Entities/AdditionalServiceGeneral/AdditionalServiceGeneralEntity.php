<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AdditionalServiceGeneral;

use Scarlet\Models\AdditionalServiceGeneral;

/**
 * @property string $OrderType
 * @property string $IntDocNumber
 * @property string $Customer
 * @property string $ServiceType
 * @property string $RecipientWarehouse
 * @property string $Recipient
 * @property string $RecipientContactName
 * @property string $RecipientPhone
 * @property string $PayerType
 * @property string $PaymentMethod
 * @property string $Note
 * @property string $RecipientSettlement
 * @property string $RecipientSettlementStreet
 * @property string $BuildingNumber
 * @property string $NoteAddressRecipient
 * @property string $Reason
 * @property string $ReturnAddressRef
 * @property string $SubtypeReason
 *
 * @property string $Ref // for delete
 * */
class AdditionalServiceGeneralEntity extends AdditionalServiceGeneral
{
    // actions

    /**
     * @return AdditionalServiceGeneralResultEntity
     * */
    public function doCreateRedirectionOrder()
    {
        return self::doSave();
    }

    /**
     * @return AdditionalServiceGeneralResultEntity
     * */
    public function doRemoveRedirectionOrder()
    {
        return self::doDelete();
    }

    /**
     * @return AdditionalServiceGeneralResultEntity
     * */
    public function doCreateReturnOrder()
    {
        return self::doSave();
    }

    /**
     * @return AdditionalServiceGeneralResultEntity
     * */
    public function doRemoveReturnOrder()
    {
        return self::doDelete();
    }

    /**
     * @return AdditionalServiceGeneralResultEntity
     * */
    public function doSave()
    {
        return new AdditionalServiceGeneralResultEntity($this->save()->jsonSerialize());
    }

    /**
     * @return AdditionalServiceGeneralResultEntity
     * */
    public function doDelete()
    {
        return new AdditionalServiceGeneralResultEntity($this->delete()->jsonSerialize());
    }
}
