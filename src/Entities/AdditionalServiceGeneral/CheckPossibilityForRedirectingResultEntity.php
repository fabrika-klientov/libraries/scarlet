<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AdditionalServiceGeneral;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $Ref
 * @property string $Number
 * @property string $PayerType
 * @property string $PaymentMethod
 * @property string $ServiceType
 * @property string $WarehouseRef
 * @property string $WarehouseDescription
 * @property string $CityRecipient
 * @property string $CityRecipientDescription
 * @property string $SettlementRecipient
 * @property string $SettlementRecipientDescription
 * @property string $SettlementType
 * @property string $CounterpartyRecipientRef
 * @property string $CounterpartyRecipientDescription
 * @property string $RecipientName
 * @property string $PhoneSender
 * @property string $PhoneRecipient
 * @property string $DocumentWeight
 * */
class CheckPossibilityForRedirectingResultEntity extends BaseEntity
{

}
