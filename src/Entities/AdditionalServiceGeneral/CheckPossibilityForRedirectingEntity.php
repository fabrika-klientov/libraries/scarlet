<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AdditionalServiceGeneral;

use Closure;
use Scarlet\Models\AdditionalServiceGeneral;

/**
 * @property int $Number
 * */
class CheckPossibilityForRedirectingEntity extends AdditionalServiceGeneral
{
    // actions

    /**
     * @param Closure|null $closure
     * @return CheckPossibilityForRedirectingResultEntity
     * */
    public function doCheckPossibilityForRedirecting(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->checkPossibilityForRedirecting($closure);
        }
        $this->currentMethod = self::CHECK_POSSIBILITY_FOR_REDIRECTING;

        return new CheckPossibilityForRedirectingResultEntity(
            $this
                ->checkAndInjectBuilderLocalData()
                ->get()
                ->jsonSerialize()
        );
    }
}
