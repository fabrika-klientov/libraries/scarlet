<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AdditionalServiceGeneral;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $FirstDayStorage
 * @property string $Total
 * @property array $Services
 * */
class AdditionalServiceGeneralPricingResultEntity extends BaseEntity
{
    /**
     * @return AdditionalServiceGeneralPricingServiceResultEntity[]
     */
    public function services(): array
    {
        return self::collectFrom('Services', AdditionalServiceGeneralPricingServiceResultEntity::class);
    }
}
