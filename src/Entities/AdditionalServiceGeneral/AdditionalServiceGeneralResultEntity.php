<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\AdditionalServiceGeneral;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $Number
 * @property string $Ref // only for create
 * @property string $ScheduledDeliveryDate
 * @property array $Pricing
 * */
class AdditionalServiceGeneralResultEntity extends BaseEntity
{
    public function pricing(): ?AdditionalServiceGeneralPricingResultEntity
    {
        return empty($this->Pricing) ? null : new AdditionalServiceGeneralPricingResultEntity($this->Pricing);
    }
}
