<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\Address;

use Closure;
use Scarlet\Models\Address;

/**
 * @property string $CityName
 * @property int $Limit
 * @property int $Page
 * */
class SettlementsEntity extends Address
{
    // actions

    /**
     * @param Closure|null $closure
     * @return \Illuminate\Support\Collection
     * */
    public function doSearchSettlements(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->searchSettlements($closure);
        }
        $this->currentMethod = self::SEARCH_SETTLEMENTS;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new SettlementsResultEntity($item->jsonSerialize());
            });
    }
}
