<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\Address;

use Closure;
use Scarlet\Models\Address;

/**
 * @property string $StreetName
 * @property string $SettlementRef
 * @property int $Limit
 * @property int $Page
 * */
class SettlementStreetsEntity extends Address
{
    // actions

    /**
     * @param Closure|null $closure
     * @return \Illuminate\Support\Collection
     * */
    public function doSearchSettlementStreets(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->searchSettlementStreets($closure);
        }
        $this->currentMethod = self::SEARCH_SETTLEMENT_STREETS;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new SettlementStreetsResultEntity($item->jsonSerialize());
            });
    }
}
