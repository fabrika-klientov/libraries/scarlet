<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\Address;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $Warehouses
 * @property string $MainDescription
 * @property string $Area
 * @property string $Region
 * @property string $SettlementTypeCode
 * @property string $Ref
 * @property string $DeliveryCity
 * */
class SettlementsResultEntity extends BaseEntity
{

}
