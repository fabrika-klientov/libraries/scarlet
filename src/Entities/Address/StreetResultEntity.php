<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\Address;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $Description
 * @property string $Ref
 * @property string $StreetsTypeRef
 * @property string $StreetsType
 * */
class StreetResultEntity extends BaseEntity
{

}
