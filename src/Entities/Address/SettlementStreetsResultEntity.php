<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\Address;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $SettlementRef
 * @property string $SettlementStreetRef
 * @property string $SettlementStreetDescription
 * @property string $Present
 * @property string $StreetsType
 * @property string $StreetsTypeDescription
 * @property string $Location
 * */
class SettlementStreetsResultEntity extends BaseEntity
{

}
