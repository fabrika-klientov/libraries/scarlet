<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\Address;

use Scarlet\Models\Address;

/**
 * @property string $CounterpartyRef
 * @property string $StreetRef
 * @property string $BuildingNumber
 * @property string $Flat
 * @property string $Note
 *
 * @property string $Ref // for update
 * */
class AddressEntity extends Address
{
    // actions

    /**
     * @return AddressResultEntity
     * */
    public function doSave()
    {
        return new AddressResultEntity($this->save()->jsonSerialize());
    }

    /**
     * @return AddressResultEntity
     * */
    public function doUpdate()
    {
        return new AddressResultEntity($this->update()->jsonSerialize());
    }

    /**
     * @return bool
     * */
    public function doDelete()
    {
        return $this->delete();
    }
}
