<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\Address;

use Scarlet\Models\Address;

/**
 * */
class AreasEntity extends Address
{
    // actions

    /**
     * @return \Illuminate\Support\Collection
     * */
    public function doAreas()
    {
        $this->areas();
        $this->currentMethod = self::AREAS;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new AreasResultEntity($item->jsonSerialize());
            });
    }
}
