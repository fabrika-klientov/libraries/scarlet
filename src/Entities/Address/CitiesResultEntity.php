<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\Address;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $Description
 * @property string $DescriptionRu
 * @property string $Ref
 * @property string $Delivery1
 * @property string $Delivery2
 * @property string $Delivery3
 * @property string $Delivery4
 * @property string $Delivery5
 * @property string $Delivery6
 * @property string $Delivery7
 * @property string $Area
 * @property string $SettlementType
 * @property string $IsBranch
 * @property string $PreventEntryNewStreetsUser
 * @property string $Conglomerates
 * @property string $CityID
 * @property string $SettlementTypeDescriptionRu
 * @property string $SettlementTypeDescription
 * */
class CitiesResultEntity extends BaseEntity
{

}
