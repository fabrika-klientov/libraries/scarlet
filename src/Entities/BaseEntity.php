<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities;

use Scarlet\Core\Classes\ActiveRecord;
use Scarlet\Core\Helpers\CollectResource;
use Scarlet\Core\Helpers\WithConstruct;

abstract class BaseEntity extends ActiveRecord
{
    use CollectResource;
    use WithConstruct;
}
