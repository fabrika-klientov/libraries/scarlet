<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\Counterparty;

use Closure;
use Scarlet\Models\Counterparty;

/**
 * @property string $FirstName
 * @property string $MiddleName
 * @property string $LastName
 * @property string $Phone
 * @property string $Email
 * @property string $CounterpartyType
 * @property string $CounterpartyProperty
 * @property string $EDRPOU
 * @property string $CityRef
 * @property string $OwnershipForm
 *
 * @property string $Ref // for update
 * */
class CounterpartyEntity extends Counterparty
{
    public const COUNTERPARTY_PROPERTY_SENDER = 'Sender';
    public const COUNTERPARTY_PROPERTY_RECIPIENT = 'Recipient';
    public const COUNTERPARTY_PROPERTY_THIRD_PERSON = 'ThirdPerson';

    public const COUNTERPARTY_TYPE_PRIVATE_PERSON = 'PrivatePerson';
    public const COUNTERPARTY_TYPE_ORGANIZATION = 'Organization';

    // actions

    /**
     * @param Closure|null $closure
     * @return \Illuminate\Support\Collection
     * */
    public function doCounterparties(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->counterparties($closure);
        }
        $this->currentMethod = self::COUNTERPARTIES;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new CounterpartyResultEntity($item->jsonSerialize());
            });
    }

    /**
     * @return CounterpartyResultEntity
     * */
    public function doSave()
    {
        return new CounterpartyResultEntity($this->save()->jsonSerialize());
    }

    /**
     * @return CounterpartyResultEntity
     * */
    public function doUpdate()
    {
        return new CounterpartyResultEntity($this->update()->jsonSerialize());
    }

    /**
     * @return bool
     * */
    public function doDelete()
    {
        return $this->delete();
    }
}
