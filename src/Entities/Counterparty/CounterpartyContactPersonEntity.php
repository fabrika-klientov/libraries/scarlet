<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\Counterparty;

use Closure;
use Scarlet\Core\Build\Builder;
use Scarlet\Models\Counterparty;

/**
 * @property string $Page
 * @property string $Ref
 * @property string $FindByString
 * */
class CounterpartyContactPersonEntity extends Counterparty
{
    // actions

    /**
     * @param Closure|null $closure
     * @return \Illuminate\Support\Collection
     * */
    public function doCounterpartyContactPersons(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->counterpartyContactPersons($closure);
        }
        $this->currentMethod = self::COUNTERPARTY_CONTACT_PERSONS;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new CounterpartyContactPersonResultEntity($item->jsonSerialize());
            });
    }
}
