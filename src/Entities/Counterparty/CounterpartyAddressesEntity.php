<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\Counterparty;

use Closure;
use Scarlet\Models\Counterparty;

/**
 * @property string $CounterpartyProperty
 * @property string $Ref
 * */
class CounterpartyAddressesEntity extends Counterparty
{
    // actions

    /**
     * @param Closure|null $closure
     * @return \Illuminate\Support\Collection
     * */
    public function doCounterpartyAddresses(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->counterpartyAddresses($closure);
        }
        $this->currentMethod = self::COUNTERPARTY_ADDRESSES;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new CounterpartyAddressesResultEntity($item->jsonSerialize());
            });
    }
}
