<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\Counterparty;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $Description
 * @property string $Ref
 * @property string $City
 * @property string $Counterparty
 * @property string $FirstName
 * @property string $LastName
 * @property string $MiddleName
 * @property string $OwnershipFormRef
 * @property string $OwnershipFormDescription
 * @property string $EDRPOU
 * @property string $CounterpartyType
 *
 * @property string $OwnershipForm // after create or update
 * @property array $ContactPerson // after create or update
 * */
class CounterpartyResultEntity extends BaseEntity
{

}
