<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\Counterparty;

use Scarlet\Entities\BaseEntity;

/**
 * @property bool $FillingWarranty
 * @property bool $AddressDocumentDelivery
 * @property bool $CanPayTheThirdPerson
 * @property bool $CanAfterpaymentOnGoodsCost
 * @property bool $CanNonCashPayment
 * @property bool $CanCreditDocuments
 * @property bool $CanEWTransporter
 * @property bool $CanSignedDocuments
 * @property bool $HideDeliveryCost
 * @property bool $BlockInternationalSenderLKK
 * @property bool $PickupService
 * @property bool $CanSameDayDelivery
 * @property bool $CanSameDayDeliveryStandart
 * @property bool $CanForwardingService
 * @property bool $ShowDeliveryByHand
 * @property bool $DeliveryByHand
 * @property bool $PartialReturn
 * @property bool $LoyaltyProgram
 * @property bool $DescentFromFloor
 * @property bool $BackDeliveryValuablePapers
 * @property bool $BackwardDeliverySubtypesDocuments
 * @property string $AfterpaymentType
 * @property bool $CreditDocuments
 * @property bool $SignedDocuments
 * @property bool $Services
 * @property bool $Debtor
 * @property bool $HaveMoneyWallets
 * */
class CounterpartyOptionsResultEntity extends BaseEntity
{

}
