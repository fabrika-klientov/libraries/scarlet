<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\Counterparty;

use Closure;
use Scarlet\Models\Counterparty;

/**
 * @property string $Ref
 * */
class CounterpartyOptionsEntity extends Counterparty
{
    // actions

    /**
     * @param Closure|null $closure
     * @return CounterpartyOptionsResultEntity
     * */
    public function doCounterpartyOptions(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->counterpartyOptions($closure);
        }
        $this->currentMethod = self::COUNTERPARTY_OPTIONS;

        return new CounterpartyOptionsResultEntity($this->checkAndInjectBuilderLocalData()->get()->jsonSerialize());
    }
}
