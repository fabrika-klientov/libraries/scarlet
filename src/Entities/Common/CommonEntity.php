<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\Common;

use Closure;
use Scarlet\Models\Common;

/**
 * @property string $RecipientCityRef // only for TimeIntervals
 * @property int $DateTime // only for TimeIntervals
 * 
 * @property int $Length // only for PackList
 * @property string $Width // only for PackList
 * @property string $Height // only for PackList
 * @property string $VolumetricWeight // only for PackList
 * @property string $TypeOfPacking // only for PackList
 *
 * @property string $FindByString // for CargoDescriptionList
 * */
class CommonEntity extends Common
{
    // actions

    /**
     * @param Closure|null $closure
     * @return \Illuminate\Support\Collection
     * */
    public function doTimeIntervals(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->timeIntervals($closure);
        }
        $this->currentMethod = self::TIME_INTERVALS;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new TimeIntervalsResultEntity($item->jsonSerialize());
            });
    }

    /**
     * @return \Illuminate\Support\Collection
     * */
    public function doCargoTypes()
    {
        $this->cargoTypes();
        $this->currentMethod = self::CARGO_TYPES;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new CargoTypesResultEntity($item->jsonSerialize());
            });
    }

    /**
     * @return \Illuminate\Support\Collection
     * */
    public function doBackwardDeliveryCargoTypes()
    {
        $this->backwardDeliveryCargoTypes();
        $this->currentMethod = self::BACKWARD_DELIVERY_CARGO_TYPES;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new BackwardDeliveryCargoTypesResultEntity($item->jsonSerialize());
            });
    }

    /**
     * @return \Illuminate\Support\Collection
     * */
    public function doPalletsList()
    {
        $this->palletsList();
        $this->currentMethod = self::PALLETS_LIST;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new PalletsListResultEntity($item->jsonSerialize());
            });
    }

    /**
     * @return \Illuminate\Support\Collection
     * */
    public function doTypesOfPayers()
    {
        $this->typesOfPayers();
        $this->currentMethod = self::TYPES_OF_PAYERS;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new TypesOfPayersResultEntity($item->jsonSerialize());
            });
    }

    /**
     * @return \Illuminate\Support\Collection
     * */
    public function doTypesOfPayersForRedelivery()
    {
        $this->typesOfPayersForRedelivery();
        $this->currentMethod = self::TYPES_OF_PAYERS_FOR_REDELIVERY;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new TypesOfPayersForRedeliveryResultEntity($item->jsonSerialize());
            });
    }

    /**
     * @param Closure|null $closure
     * @return \Illuminate\Support\Collection
     * */
    public function doPackList(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->packList($closure);
        }
        $this->currentMethod = self::PACK_LIST;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new PackListResultEntity($item->jsonSerialize());
            });
    }

    /**
     * @return \Illuminate\Support\Collection
     * */
    public function doTiresWheelsList()
    {
        $this->tiresWheelsList();
        $this->currentMethod = self::TIRES_WHEELS_LIST;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new TiresWheelsListResultEntity($item->jsonSerialize());
            });
    }

    /**
     * @param Closure|null $closure
     * @return \Illuminate\Support\Collection
     * */
    public function doCargoDescriptionList(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->cargoDescriptionList($closure);
        }
        $this->currentMethod = self::CARGO_DESCRIPTION_LIST;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new CargoDescriptionListResultEntity($item->jsonSerialize());
            });
    }

    /**
     * @return \Illuminate\Support\Collection
     * */
    public function doServiceTypes()
    {
        $this->serviceTypes();
        $this->currentMethod = self::SERVICE_TYPES;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new ServiceTypesResultEntity($item->jsonSerialize());
            });
    }

    /**
     * @return \Illuminate\Support\Collection
     * */
    public function doTypesOfCounterparties()
    {
        $this->typesOfCounterparties();
        $this->currentMethod = self::TYPES_OF_COUNTERPARTIES;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new TypesOfCounterpartiesResultEntity($item->jsonSerialize());
            });
    }

    /**
     * @return \Illuminate\Support\Collection
     * */
    public function doPaymentForms()
    {
        $this->paymentForms();
        $this->currentMethod = self::PAYMENT_FORMS;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new PaymentFormsResultEntity($item->jsonSerialize());
            });
    }

    /**
     * @return \Illuminate\Support\Collection
     * */
    public function doOwnershipFormsList()
    {
        $this->ownershipFormsList();
        $this->currentMethod = self::OWNERSHIP_FORMS_LIST;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new OwnershipFormsListResultEntity($item->jsonSerialize());
            });
    }
}
