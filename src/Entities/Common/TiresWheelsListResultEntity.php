<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\Common;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $Ref
 * @property string $Description
 * @property string $DescriptionRu
 * @property string $Weight
 * @property string $DescriptionType
 * */
class TiresWheelsListResultEntity extends BaseEntity
{

}
