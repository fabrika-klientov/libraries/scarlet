<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 23.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\International;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $CountryRef
 * @property string $City
 * @property string $ZipCode
 * @property array $WarehouseOwners
 * @property array $WarehouseCategories
 * @property bool $HavePoshtomats
 * @property bool $HaveWarehouses
 * */
class CityResultEntity extends BaseEntity
{

}
