<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 23.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\International;

use Scarlet\Models\International;

/**
 * */
class CountryEntity extends International
{
    // actions

    /**
     * @return \Illuminate\Support\Collection
     * */
    public function doCountries()
    {
        $this->countries();
        $this->currentMethod = self::COUNTRIES;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new CountryResultEntity($item->jsonSerialize());
            });
    }
}
