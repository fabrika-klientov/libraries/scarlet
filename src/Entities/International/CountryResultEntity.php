<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 23.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\International;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $Description
 * @property string $Ref
 * @property string $Code
 * @property string $DescriptionRu
 * @property string $DescriptionEn
 * @property string $CallingCode
 * @property string $DefaultZipCode
 * @property array $Settings
 * >> $Settings array of objects:
 * "CounterpartyType": "Organization",
 * "DeliveryType": "fe09922d-a212-11eb-94e4-b8830365bd14",
 * "DeliveryTypeDescription": "Глобальна доставка преміум",
 * "Direction": "Export",
 * "ShipmentType": "b12b3781-70d6-11e8-9296-0025b502a04e",
 * "AmountDaysOfDelivery": "8",
 * "MaxGirthCm": "0.0",
 * "MinLengthCm": "0.0",
 * "MinWidthCm": "0.0",
 * "MinHeightCm": "0.0",
 * "MaxLengthCm": "120.0",
 * "MaxWidthCm": "80.0",
 * "MaxHeightCm": "69.0",
 * "MinWeight": "0.000",
 * "MaxWeight": "5.000",
 * "MaxWeightForSumAllPlaces": "1",
 * "PlacesAmount": "0",
 * "PaymentCurrency": "b12baf9d-70d6-11e8-9296-0025b502a04e",
 * "MaxEstimatedCost": "0.00",
 * "DeliveredCargoToWarehouseDPD": "0",
 * "MaxVolumetricWeight": "5.00",
 * "MaxVolumetricWeightForSumAllPlaces": "1",
 * */
class CountryResultEntity extends BaseEntity
{

}
