<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 23.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\International;

use Closure;
use Scarlet\Models\International;

/**фра
 * */
class CityEntity extends International
{
    // actions

    /**
     * @return \Illuminate\Support\Collection
     * */
    public function doCities(Closure $closure = null)
    {
        if (isset($closure)) {
            $this->cities($closure);
        }
        $this->currentMethod = self::CITIES;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new CityResultEntity($item->jsonSerialize());
            });
    }
}
