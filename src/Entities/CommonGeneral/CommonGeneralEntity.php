<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\CommonGeneral;

use Scarlet\Models\CommonGeneral;

/**
 * */
class CommonGeneralEntity extends CommonGeneral
{
    // actions

    /**
     * @return \Illuminate\Support\Collection
     * */
    public function doMessageCodeText()
    {
        $this->messageCodeText();
        $this->currentMethod = self::MESSAGE_CODE_TEXT;

        return $this
            ->checkAndInjectBuilderLocalData()
            ->get()
            ->map(function (self $item) {
                return new MessageCodeTextResultEntity($item->jsonSerialize());
            });
    }
}
