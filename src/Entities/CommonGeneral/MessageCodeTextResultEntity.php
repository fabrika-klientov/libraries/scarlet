<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Entities
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Entities\CommonGeneral;

use Scarlet\Entities\BaseEntity;

/**
 * @property string $MessageCode
 * @property string $MessageText
 * @property string $MessageDescriptionRU
 * @property string $MessageDescriptionUA
 * */
class MessageCodeTextResultEntity extends BaseEntity
{

}
