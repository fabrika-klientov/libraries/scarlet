<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.10
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Core\Build;

use Illuminate\Support\Str;

/**
 * @method $this ref(string $value) Ref helper set
 * @method $this country(string $value) Country Ref helper set
 * @method $this cityRef(string $value) CityRef helper set
 * @method $this cityName(string $value) CityName helper set
 * @method $this areaRef(string $value) AreaRef helper set
 * @method $this regionRef(string $value) RegionRef helper set
 * @method $this streetName(string $value) StreetName helper set
 * @method $this streetRef(string $value) StreetRef helper set
 * @method $this findByString(mixed $value) FindByString helper set
 * @method $this settlementRef(string $value) SettlementRef helper set
 * @method $this counterpartyRef(string $value) CounterpartyRef helper set
 * @method $this note(string $value) Note helper set
 * @method $this limit(int $value) Limit
 * @method $this page(int $value) Page
 * @method $this dateTimeFrom(string $value) DateTimeFrom
 * @method $this dateTimeTo(string $value) DateTimeTo
 * @method $this dateTime(string $value) DateTime
 * */
class Builder
{
    /**
     * @var array $data
     * */
    protected $data;

    /**
     * @param array|null $whereData
     */
    public function __construct(array $whereData = null)
    {
        $this->data = $whereData ?? [];
    }

    /** where builder cl
     * @param string $key
     * @param mixed $value
     * @return $this
     * */
    public function where($key, $value)
    {
        $this->data[$key] = $value;

        return $this;
    }

    /** clear filter
     * @return $this
     * */
    public function clear()
    {
        $this->data = [];
        return $this;
    }

    /** get filter
     * @return array
     * */
    public function getResult()
    {
        return $this->data;
    }

    /** has key property
     * @param string $key
     * @return bool
     * */
    public function has($key)
    {
        return !empty($this->data[$key]);
    }

    /** check if empty
     * @return bool
     * */
    public function isEmpty()
    {
        return empty($this->data);
    }

    /** mixed property where
     * @param string $name
     * @param array $arguments
     * @return $this
     * */
    public function __call($name, $arguments)
    {
        $this->where(Str::ucfirst($name), ...$arguments);

        return $this;
    }

    /** !!
     * @deprecated
     * @param array $array
     * @param array $mergeArray
     * @return void
     * */
    protected function merge(&$array, $mergeArray)
    {
        $array = array_merge($array, $mergeArray);
    }
}
