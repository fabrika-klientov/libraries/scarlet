<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Core\Helpers;

use Illuminate\Support\Collection;
use Scarlet\Exceptions\ScarletException;

trait CollectResource
{
    /**
     * @param string $key
     * @param string $className
     * @param bool $isCollection
     * @return array|Collection
     *
     * @throws ScarletException
     */
    protected function collectFrom(string $key, string $className, bool $isCollection = false)
    {
        if (!class_exists($className)) {
            throw new ScarletException("Class name [$className] is not resolved");
        }

        $list = array_map(function ($item) use ($className) {
            return new $className($item);
        }, $this->{$key} ?? []);

        if ($isCollection) {
            return new Collection($list);
        }

        return $list;
    }
}
