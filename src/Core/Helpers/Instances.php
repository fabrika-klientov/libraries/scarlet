<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.10
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Core\Helpers;

use Illuminate\Support\Str;

/**
 * @property-read \Scarlet\Models\Address $address
 * @property-read \Scarlet\Models\AddressGeneral $addressGeneral
 * @property-read \Scarlet\Models\Counterparty $counterparty
 * @property-read \Scarlet\Models\ContactPerson $contactPerson
 * @property-read \Scarlet\Models\InternetDocument $internetDocument
 * @property-read \Scarlet\Models\TrackingDocument $trackingDocument
 * @property-read \Scarlet\Models\Common $common
 * @property-read \Scarlet\Models\CommonGeneral $commonGeneral
 * @property-read \Scarlet\Models\PrintForms $printForms
 * @property-read \Scarlet\Models\ScanSheet $scanSheet
 * @property-read \Scarlet\Models\AdditionalService $additionalService
 * @property-read \Scarlet\Models\AdditionalServiceGeneral $additionalServiceGeneral
 * @property-read \Scarlet\Models\AddressContactPersonGeneral $addressContactPersonGeneral
 * @property-read \Scarlet\Models\International $international
 * */
trait Instances
{
    /** getter magic instances
     * @param string $name
     * @return \Scarlet\Models\Model|null
     * */
    public function __get($name)
    {
        switch ($name) {
            case 'address':
            case 'addressGeneral':
            case 'counterparty':
            case 'contactPerson':
            case 'internetDocument':
            case 'trackingDocument':
            case 'common':
            case 'commonGeneral':
            case 'printForms':
            case 'scanSheet':
            case 'additionalService':
            case 'additionalServiceGeneral':
            case 'addressContactPersonGeneral':
            case 'international':
                $class = 'Scarlet\\Models\\' . Str::ucfirst($name);
                return new $class($this->httpClient);
        }

        return null;
    }
}
