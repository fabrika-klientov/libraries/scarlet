<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Core\Helpers;

trait WithConstruct
{
    protected function with(string $key, $value)
    {
        $this->setValue($key, $value);

        return $this;
    }

    protected function setValue(string $key, $value)
    {
        $this->data[$key] = $value;
    }
}
