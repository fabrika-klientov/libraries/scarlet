<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 20.09.18
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Core\Query;

use Scarlet\Exceptions\ScarletException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class HttpClient
{
    /**
     * @var string $token
     * */
    private $token;

    /**
     * @var string $LINK
     * */
    private static $LINK = 'https://api.novaposhta.ua/v2.0/json/';

    /**
     * @var string $MY_LINK
     * */
    public static $MY_LINK = 'https://my.novaposhta.ua/';

    public const TIMEOUT = 30;

    /**
     * @param string $token
     * @return void
     * */
    public function __construct(string $token)
    {
        $this->token = $token;
    }

    /** api GET
     * @param string $link
     * @param array $options
     * @return array|null
     * @throws ScarletException
     * */
    public function get($link, $options)
    {
        return $this->request('GET', $link, $options);
    }

    /** api POST
     * @param string $link
     * @param array $options
     * @return array|null
     * @throws ScarletException
     * */
    public function post($link, $options)
    {
        return $this->request('POST', $link, $options);
    }

    /** NP token
     * @return string
     * */
    public function getToken()
    {
        return $this->token;
    }

    /** full url
     * @param string $link
     * @return string
     * */
    public function getURL(string $link = '')
    {
        return self::$LINK . $link;
    }

    /**
     * @return Client
     * */
    public function getClient(): Client
    {
        return new Client(
            [
                'base_uri' => self::$LINK,
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
                'timeout' => self::TIMEOUT,
            ]
        );
    }

    /**
     * @param string $method
     * @param string $link
     * @param array $options
     * @return array|string|null
     * @throws ScarletException
     * */
    public function request(string $method, string $link, array $options, bool $src = false)
    {
        try {
            $response = $this->getClient()->request($method, $link, $options)->getBody()->getContents();

            if ($src) {
                return $response;
            }

            $result = json_decode($response, true);

            if (!empty($result)) {
                if ($result['success'] || !empty($result['data'])) { // InternetDocument::generateReport return false
                    return $result;
                }

                throw (new ScarletException(
                    'NP resulted status error. ' . json_encode($result['errors'])
                ))->setDataError($result);
            }
        } catch (ScarletException $scarletException) {
            throw (new ScarletException('Request returned error. ' . $scarletException->getMessage()))
                ->setDataError($scarletException->getDataError());
        } catch (ClientException $clientException) {
            throw new ScarletException(
                'Request returned error. ' . $clientException->getMessage(),
                $clientException->getCode()
            );
        } catch (\Exception $exception) {
            throw new ScarletException('Request returned error. ' . $exception->getMessage());
        }

        return null;
    }
}
