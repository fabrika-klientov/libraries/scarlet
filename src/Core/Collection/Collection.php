<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.10
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Core\Collection;

use Illuminate\Support\Collection as BaseCollection;

class Collection extends BaseCollection
{
    /** mass save
     * @return void
     * */
    public function save()
    {
        $this->each(function ($item) {
            if (method_exists($item, 'save')) {
                $item->save();
            }
        });
    }

    /** mass update
     * @return void
     * */
    public function update()
    {
        $this->each(function ($item) {
            if (method_exists($item, 'update')) {
                $item->update();
            }
        });
    }

    /** mass delete
     * @return void
     * */
    public function delete()
    {
        $this->each(function ($item) {
            if (method_exists($item, 'delete')) {
                $item->delete();
            }
        });
    }
}
