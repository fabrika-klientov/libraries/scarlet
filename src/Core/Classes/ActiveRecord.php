<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Core
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2021 Fabrika-Klientov
 * @version   GIT: 21.10.21
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Core\Classes;

use JsonSerializable;

abstract class ActiveRecord implements JsonSerializable
{
    /**
     * @var array $data
     * */
    protected $data = [];

    /**
     * @param array $data
     * @return void
     * */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /** isset
     * @param string $name
     * @return bool
     * */
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    /** getter
     * @param string $name
     * @return mixed
     * */
    public function __get($name)
    {
        return $this->data[$name] ?? null;
    }

    /** setter
     * @param string $name
     * @param mixed $value
     * @return void
     * */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    /** serializable
     * @return string
     * */
    public function __toString()
    {
        return (string)json_encode($this->data);
    }

    /** \JsonSerializable
     * @return array
     * */
    public function jsonSerialize()
    {
        return $this->data;
    }
}
