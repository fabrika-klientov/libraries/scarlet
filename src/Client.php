<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Scarlet
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 19.10.10
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet;

use Scarlet\Core\Helpers\Instances;
use Scarlet\Core\Query\HttpClient;

class Client
{
    use Instances;

    /**
     * @var HttpClient $httpClient
     * */
    private $httpClient;

    /**
     * @var string $token
     * */
    private $token;

    /**
     * @param string $token
     * @return void
     * */
    public function __construct(string $token)
    {
        $this->token = $token;
        $this->httpClient = new HttpClient($this->token);
    }

    /**
     * @return HttpClient
     * */
    public function getHttpClient()
    {
        return $this->httpClient;
    }
}
