<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Scarlet
 * @category  Exceptions
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 20.02.11
 * @link      https://fabrika-klientov.ua
 */

namespace Scarlet\Exceptions;

use Throwable;

class ScarletException extends \Exception
{
    /**
     * @var array $dataError
     * */
    protected $dataError = [];

    /**
     * @var string $logPrefix
     * */
    protected static $logPrefix = 'SCARLET::CORE: ';

    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->setMessage($message);
    }

    /**
     * @param mixed $data
     * @return $this
     */
    public function setDataError($data)
    {
        $this->dataError = $data ?? [];
        return $this;
    }

    /**
     * @return array
     */
    public function getDataError()
    {
        return $this->dataError;
    }

    /**
     * @return array
     */
    public function getErrorCodes()
    {
        return $this->dataError['errorCodes'] ?? [];
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->dataError['errors'] ?? [];
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message): void
    {
        $this->message = static::$logPrefix . str_replace(static::$logPrefix, '', $message);
    }
}
