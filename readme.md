## Status
       
[![Latest Stable Version](https://poser.pugx.org/fbkl/scarlet/v/stable)](https://packagist.org/packages/fbkl/scarlet)
[![pipeline status](https://gitlab.com/fabrika-klientov/libraries/scarlet/badges/master/pipeline.svg)](https://gitlab.com/fabrika-klientov/libraries/scarlet/commits/master)
[![License](https://poser.pugx.org/fbkl/scarlet/license)](https://packagist.org/packages/fbkl/scarlet)

Библиотека для работы с API Новой Почты

---

## Usage

`composer require fbkl/scarlet`

---

## Работа с библиотекой

### Update

**Добавлены сущности разных моделей для удобства построения запросов
(прежний тип (способ) запросов не изменился)**


### Client (Клиент для работы с НП)

**В самом начале: Для того чтобы не возникало лишних вопросов, желательно ознакомится с оффициальной документацией
Новой Почты. `1.` Так как многие методы и свойства имеют логически такое же названия как и удаленные методы
Новой Почты. `2.` Проверки в библиотеке усть в полной мере, но вы должны понимать какие необходимые данные 
должны отправлять в той или инной удаленный метод Новой Почты. `3.` Так же - разрешения (права) и ограничения
Новой Почты лежат на плечах пользователя данной библиотекой.**

Для начала работы с быблиотекой Новой Почты (далее НП) необходимо создать инстанс клиента, передав ему 
ваш токен ключ (API ключ)

```php
$client = new \Scarlet\Client('b59e07c30djdfjkdfjk1910330ff8c4');

```

далее вы можете вызывать инстансы нужных вам моделей для конкретных получения, создания, изменения, удаления данных

```php
// .. $client = new ...

/**
 * @var \Scarlet\Models\Address $address
 */
$address = $client->address;

/**
 * @var \Scarlet\Models\InternetDocument $internetDocument
 */
$internetDocument = $client->internetDocument;

// .. другие так же

```


Для удобства понимания материал будет изложен примерно в той же последовательности как и оффициальная док.
НП.

Если данные которые могут быть в количестве более 1 модели - будут помещены в коллекции (даже если там 
будет одна модель, или коллекция будет пустой) - обьект `\Scarlet\Core\Collection\Collection` наследуемый
от `\Illuminate\Support\Collection` соответственно методов для работы с коллекциями есть более чем достаточно: 
[Illuminate\Support\Collection](https://laravel.com/api/5.5/Illuminate/Support/Collection.html).

Коллекции и все модели библиотеки - сериализируются, что дает легкий механизм как в разработке (тестировании)
так и выдачу результата вашему frontend приложению.

Все запросы могут выбрасывать исключения (в основном `\Scarlet\Exceptions\ScarletException`). Хоть и
НП всегда возвращает ответ `200` - исключения могут быть еще до запроса, и(или) если НП вернула
ответ с статусом ошибки. Ловить исключения - на плечи разработчика.

### Коллекции

Как описано выше если метод возвращает список данных - они будут помещены в коллекции.

Отличие от коллекций `Illuminate\Support\Collection` то что `\Scarlet\Core\Collection\Collection`
могут массово создавать, обновлять, удалять некие модели (которые могут это делать). Так как НП
не разрешает массовые действия - все действия (создавать, обновлять, удалять) будут пропущены в цыкле.
Соответственно это займет немалое время для большого количества моделей в коллекции. Возможно у НП есть 
ограничение на количество запросов в сек. и другие, учтите это. 

```php

$collect = new \Scarlet\Core\Collection\Collection();

$collect->add(
    new \Scarlet\Models\InternetDocument($client->getHttpClient(), [
        "PayerType" => "Sender",
        "PaymentMethod" => "Cash",
        // .... first
    ])
);

$collect->add(
    new \Scarlet\Models\InternetDocument($client->getHttpClient(), [
        "PayerType" => "Sender",
        "PaymentMethod" => "Cash",
        // .... second
    ])
);
// создаем данные коллекции
$collect->save();

$collection = $client->internetDocument->documentList(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->dateTimeFrom('01.10.2019')->dateTimeTo('10.10.2019');
})->get();

$collection->each(function($item) {
    $item->Weight = '20';
});

// обновляем коллекцию
$collection->update();
// удаляем коллекцию
$collection->delete();

```

### Работа с адресами (API Адреса)

Для работы в этом разделе существуют 2 модели: 
- `\Scarlet\Models\Address`,
- `\Scarlet\Models\AddressGeneral`.

```php

/**
 * @var \Scarlet\Models\Address $address
 */
$address = $client->address;

/** Онлайн поиск в справочнике населенных пунктов
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\Address> $collection
 */
$collection = $address->searchSettlements(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->cityName('київ')->limit(35);
})->get();

/** Онлайн поиск улиц в справочнике населенных пунктов
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\Address> $collection
 */
$collection = $address->searchSettlementStreets(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->streetName('Незалежност')->settlementRef('e715719e-4b33-11e4-ab6d-005056801329')->limit(35);
})->get();

/** Справочник географических областей Украины
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\Address> $collection
 */
$collection = $address->areas()->get();

/** Справочник городов компании
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\Address> $collection
 */
// все 
$collection = $address->cities()->get();
// или 
$collection = $address->cities(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->ref('ebc0eda9-93ec-11e3-b441-0050568002cf');
})->get();

/** Справочник улиц компании
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\Address> $collection
 */
$collection = $address->street(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->cityRef('ebc0eda9-93ec-11e3-b441-0050568002cf');
})->get();


// работа с коллекцией (любой) все методы в офф. док. 

// выведет в браузер и серриализует все данные
echo $collection;
// выведем примитивный список 
$collection->each(function(\Scarlet\Models\Address $item) { // или другая модель в другом контексте
    echo '- ';
    echo $item->Description;
    echo '<br>';
});

// Создать адрес контрагента (отправитель/получатель)
// создаем модель
$model1 = new \Scarlet\Models\Address($client->getHttpClient(), [
    'CounterpartyRef' => '29f3b2f7-153c-11e7-8ba8-005056881c6b',
    'StreetRef' => 'd4450bdb-0a58-11de-b6f5-001d92f78697',
    'BuildingNumber' => '77',
    'Flat' => '56',
    'Note' => 'test 321',
]);
// можно создать так
$model2 = new \Scarlet\Models\Address($client->getHttpClient());
$model2->CounterpartyRef = '29f3b2f7-153c-11e7-8ba8-005056881c6b';
$model2->StreetRef = 'd4450bdb-0a58-11de-b6f5-001d92f78697';
$model2->BuildingNumber = '77';
$model2->Flat = '56';
$model2->Note = 'дополнительно';
// или даж так 
$model3 = $client->address;
$model3->CounterpartyRef = '---';
// .....

$model1->save();
$model2->save();
$model3->save();

// Редактировать адрес контрагента (отправитель/получатель) (работает если у вас коллекция адресов контрагента)
$model = $collection->first();
$model->BuildingNumber = '78';
$model->update();

// Удалить адрес контрагента (отправитель/получатель) (работает если у вас коллекция адресов контрагента)
$model = $collection->first();
$model->delete();


/**
 * @var \Scarlet\Models\AddressGeneral $addressGeneral
 */
$addressGeneral = $client->addressGeneral;

/** Справочник населенных пунктов Украины
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\AddressGeneral> $collection
 */
$collection = $address->settlements(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->areaRef('dcaadb64-4b33-11e4-ab6d-005056801329')->page(1);
})->get();

/** Справочник отделений 
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\AddressGeneral> $collection
 */
$collection = $address->warehouses(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->where('Language', 'ru');
})->get();

/** Справочник типов отделений 
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\AddressGeneral> $collection
 */
$collection = $address->warehouseTypes()->get();

```

Как вы заметили в фильтрации любых запросов (и не только модели `\Scarlet\Models\Address`) исползуется 
лямбда выражение с обьектом `\Scarlet\Core\Build\Builder $builder` который в себе имеет немало методов
установки фильтра, в случае если необходимого фильтра нету (а это так и есть) вы можете использовать
метод `where('CityRef', 'ebc0eda9-93ec-11e3-b441-0050568002cf');`.


### Работа с контрагентами (API Контрагенты)

Для работы в этом разделе существуют 2 модели: 
- `\Scarlet\Models\Counterparty`,
- `\Scarlet\Models\ContactPerson`.

```php

/**
 * @var \Scarlet\Models\Counterparty $counterparty
 */
$counterparty = $client->counterparty;

/** Загрузить список адресов Контрагентов
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\Counterparty> $collection
 */
$collection = $counterparty->counterpartyAddresses(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->ref('5ace4a2e-13ee-11e5-add9-005056887b8d')->CounterpartyProperty('Sender');
})->get();

/** Загрузить параметры Контрагента
 * @var \Scarlet\Models\Counterparty $model
 */
$model = $counterparty->counterpartyOptions(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->ref('5ace4a2e-13ee-11e5-add9-005056887b8d');
})->get();

/** Загрузить список контактных лиц Контрагента
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\Counterparty> $collection
 */
$collection = $counterparty->counterpartyContactPersons(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->ref('5ace4a2e-13ee-11e5-add9-005056887b8d')->page(2);
})->get();

/** Загрузить список Контрагентов отправителей/получателей/третье лицо
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\Counterparty> $collection
 */
$collection = $counterparty->counterparties(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->where('CounterpartyProperty', 'Sender');
})->get();

/** Создать Контрагента
 * @var \Scarlet\Models\Counterparty $model
 */
$model = $client->counterparty;
$model->CounterpartyType = 'PrivatePerson'; // PrivatePerson | Organization
$model->CounterpartyProperty = 'Recipient';
$model->FirstName = 'Гриша';
$model->LastName = 'Василенко';
$model->Phone = '0999999999';
$model->save();

// Создать Контрагента с типом (юридическое лицо) или организацию
// и Создать Контрагента с типом третьего лица так же как и Создать Контрагента, 
// обязательно смотрите какие поля должны быть заполнены, а которые должны отсутствовать
// Так же модель можно создать способами описаных в разделе <Работа с адресами (API Адреса)>

/** Обновить данные Контрагента (если в коллекции список Контрагентов отправителей/получателей/третье лицо)
 * @var \Scarlet\Models\Counterparty $model
 */
$model = $collection->first();
$model->MiddleName = 'Іванович';
$model->update();

/** Удалить Контрагента получателя (если в коллекции список Контрагентов отправителей/получателей/третье лицо)
 * @var bool $status
 */
$model = $collection->first();
$status = $model->delete();

/** Создать Контактное лицо Контрагента
 * @var \Scarlet\Models\ContactPerson $model
 */
$model = new \Scarlet\Models\ContactPerson($client->getHttpClient(), [
    "CounterpartyRef" => "768da332-159d-11e5-ad08-005056801333",
    "FirstName" => "Люцифер",
    "LastName" => "Кравченко",
    "MiddleName" => "Борисович",
    "Phone" => "+380997979789",
]);
$model->save();

/** Обновить данные контактного лица Контрагента (без получения модели или коллекции)
 * @var \Scarlet\Models\Counterparty $model
 */
$model = new \Scarlet\Models\ContactPerson($client->getHttpClient(), [
    "CounterpartyRef" => "768da332-159d-11e5-ad08-005056801333",
    "Ref" => "9ad69c2b-159d-11e5-ad08-005056801333", // <-
    "FirstName" => "Люцифер",
    "LastName" => "Кравченко",
    "MiddleName" => "НЕБорисович",
    "Phone" => "+380997979789",
]);
$model->update();

/** Удалить Контактное лицо Контрагента (без получения модели или коллекции)
 * @var bool $status
 */
$model = new \Scarlet\Models\ContactPerson($client->getHttpClient());
$model->Ref = "9ad69c2b-159d-11e5-ad08-005056801333";
$status = $model->delete();

```

### Работа с Печатные формы (API Печатные формы)

Для работы в этом разделе существует 1 модель: 
- `\Scarlet\Models\PrintForms`,

Все методы (3 шт.) возвращают необходимые ссылки для загрузки (печати) форм

```php

/**
 * @var \Scarlet\Models\PrintForms $printForms
 */
$printForms = $client->printForms;

// Маркировки - печатная форма
// возвращает ссылку (string) (обязателен параметр только - orders)
$link = $printForms->markings(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->where('orders', [
        '20450170569531',
        '7f9ec785-eb38-11e9-9937-005056881c6b',
    ])
        ->where('type', 'html')
        ->where('is100', true)
        ->where('zebra', true);
});

// Реестры - печатная форма
// возвращает ссылку (string) (обязателен параметр только - refs)
$link = $printForms->registry(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->where('refs', [
        '7ed08016-89e6-11e5-a70c-005056801333',
    ]);
});

// Экспресс-накладная - печатные формы
// возвращает ссылку (string) (обязателен параметр только - orders)
$link = $printForms->internetDocument(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->where('orders', [
        '20450170569531',
        '7f9ec785-eb38-11e9-9937-005056881c6b',
    ])
        ->where('type', 'pdf');
});

```

### Работа с Реестрами (API Реестры)

Для работы в этом разделе существует 1 модель: 
- `\Scarlet\Models\ScanSheet`,

```php

/**
 * @var \Scarlet\Models\ScanSheet $scanSheet
 */
$scanSheet = $client->scanSheet;

/** Загрузить список реестров
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\ScanSheet> $collection
 */
$collection = $scanSheet->scanSheetList()->get();

/** Загрузить информацию по одному реестру
 * @var \Scarlet\Models\ScanSheet $model
 */
$model = $scanSheet->scanSheet(function (\Scarlet\Core\Build\Builder $builder) {
   $builder->ref('82449419-6091-11e7-8ba8-005056881c6b');
})->get();

/** Добавить экспресс-накладные
 * есть 2 способа добавить ЭН в реестр
 * @var \Scarlet\Models\ScanSheet $result
 */
// 1
$result = $scanSheet->insertDocuments(function (\Scarlet\Core\Build\Builder $builder) {
   $builder->where('DocumentRefs', [
           '7f9ec785-eb38-11e9-9937-005056881c6b',
           '736e0192-eb38-11e9-9937-005056881c6b',
           '8350c9bf-eb2f-11e9-9937-005056881c6b',
       ]
   )->ref('da4d410a-eb5f-11e9-9937-005056881c6b');
})->set();
// 2 (передать параметром коллекцию)
$collection = new \Scarlet\Core\Collection\Collection([
    new \Scarlet\Models\InternetDocument(
        $client->getHttpClient(),
        ['Ref' => '7f9ec785-eb38-11e9-9937-005056881c6b']
    ),
    new \Scarlet\Models\InternetDocument(
        $client->getHttpClient(),
        ['Ref' => '7f9ec785-eb40-11e9-9937-005056881c6b']
    ),
]);

$result = $scanSheet->insertDocuments($collection)->set();
// но в этом случае вы не сможете добавить ЭН в конкретный реестр (а в новый),
// и использовать параметр Date так же нельзя

/** Удалить экспресс-накладные из реестра
 * есть 2 способа удалить ЭН из реестра
 * @var \Scarlet\Models\ScanSheet $result
 */
// 1
$result = $scanSheet->removeDocuments(function (\Scarlet\Core\Build\Builder $builder) {
   $builder->where('DocumentRefs', [
           '7f9ec785-eb38-11e9-9937-005056881c6b',
           '736e0192-eb38-11e9-9937-005056881c6b',
       ]
   )->ref('da4d410a-eb5f-11e9-9937-005056881c6b');
})->set();
// 2 (передать параметром коллекцию)
$collection = new \Scarlet\Core\Collection\Collection([
    new \Scarlet\Models\InternetDocument(
        $client->getHttpClient(),
        ['Ref' => '7f9ec785-eb38-11e9-9937-005056881c6b']
    ),
    new \Scarlet\Models\InternetDocument(
        $client->getHttpClient(),
        ['Ref' => '7f9ec785-eb40-11e9-9937-005056881c6b']
    ),
]);

$result = $scanSheet->removeDocuments($collection)->set();
// но в этом случае вы не сможете указать Ref реестра

/** Удалить (расформировать) реестр отправлений
 * есть 2 способа удалить (расформировать) реестр
 * @var \Scarlet\Models\ScanSheet $result
 */
// 1
$result = $scanSheet->deleteScanSheet(function (\Scarlet\Core\Build\Builder $builder) {
   $builder->where('ScanSheetRefs', [
           '0d50bf64-eb62-11e9-9937-005056881c6b',
       ]
   );
})->set();
// 2 (передать параметром коллекцию)
$collection = new \Scarlet\Core\Collection\Collection([
    new \Scarlet\Models\ScanSheet(
        $client->getHttpClient(),
        ['Ref' => 'c26cfb3d-eb61-11e9-9937-005056881c6b']
    ),
    new \Scarlet\Models\ScanSheet(
        $client->getHttpClient(), 
        ['Ref' => 'acc6e35a-eb61-11e9-9937-005056881c6b']
    ),
]);

$result = $scanSheet->deleteScanSheet($collection)->set();

```

### Работа с Справочниками (API Справочники)

Для работы в этом разделе существуют 2 модели: 
- `\Scarlet\Models\Common`,
- `\Scarlet\Models\CommonGeneral`.

```php

/**
 * @var \Scarlet\Models\Common $common
 */
$common = $client->common;

/** Виды временных интервалов
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\Common> $collection
 */
$collection = $common->timeIntervals(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->where('RecipientCityRef', '8d5a980d-391c-11dd-90d9-001a92567626');
})->get();

/** Виды груза
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\Common> $collection
 */
$collection = $common->cargoTypes()->get();

/** Виды обратной доставки груза
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\Common> $collection
 */
$collection = $common->backwardDeliveryCargoTypes()->get();

/** Виды паллет
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\Common> $collection
 */
$collection = $common->palletsList()->get();

/** Виды плательщиков
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\Common> $collection
 */
$collection = $common->typesOfPayers()->get();

/** Виды плательщиков обратной доставки
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\Common> $collection
 */
$collection = $common->typesOfPayersForRedelivery()->get();

/** Виды упаковки (условие необязательно)
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\Common> $collection
 */
$collection = $common->packList(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->where('Length', '2')->where('Width', '1');
})->get();

/** Виды шин и дисков
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\Common> $collection
 */
$collection = $common->tiresWheelsList()->get();

/** Описания груза (условие необязательно)
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\Common> $collection
 */
$collection = $common->cargoDescriptionList(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->findByString('абаж');
})->get();

/** Технологии доставки
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\Common> $collection
 */
$collection = $common->serviceTypes()->get();

/** Типы контрагентов
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\Common> $collection
 */
$collection = $common->typesOfCounterparties()->get();

/** Формы оплаты
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\Common> $collection
 */
$collection = $common->paymentForms()->get();

/** Формы собственности
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\Common> $collection
 */
$collection = $common->ownershipFormsList()->get();

/** Перечень ошибок
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\CommonGeneral> $collection
 */
$collection = $client->commonGeneral->messageCodeText()->get();

```


### Работа с возврат отправления (API Услуга возврат отправления)

Для работы в этом разделе существует 1 модель: 
- `\Scarlet\Models\AdditionalService`,

```php

/**
 * @var \Scarlet\Models\AdditionalService $additionalService
 */
$additionalService = $client->additionalService;

/** Проверка возможности создания заявки на возврат
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\AdditionalService> $collection
 */
$collection = $additionalService->checkPossibilityCreateReturn(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->where('Number', '20450170540754');
})->get();

/** Получение списка причин возврата
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\AdditionalService> $collection
 */
$collection = $additionalService->returnReasons()->get();

/** Получение списка подтипов причины возврата (условие опционально)
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\AdditionalService> $collection
 */
$collection = $additionalService->returnReasonsSubtypes(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->where('ReasonRef', '49754eb2-a9e1-11e3-9fa0-0050568002cf');
})->get();

/** Получение списка заявок на возврат (условие опционально)
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\AdditionalService> $collection
 */
$collection = $additionalService->returnOrdersList(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->where('ReasonRef', '49754eb2-a9e1-11e3-9fa0-0050568002cf');
})->get();

/** Создание заявки на возврат
 * @var \Scarlet\Models\AdditionalService $model
 */
$model = new \Scarlet\Models\AdditionalService($client->getHttpClient(), [
    "IntDocNumber" => "20450170540754",
    "PaymentMethod" => "Cash",
    "Reason" =>"49754eb2-a9e1-11e3-9fa0-0050568002cf",
    "SubtypeReason" => "faaeb2b9-1d6d-11e4-acce-0050568002cf",
    "Note" => "Произвольное описание",
    "OrderType" => "orderCargoReturn",
    "ReturnAddressRef" => "6cec19e5-cb80-11e6-aaab-005056801329"
]);
$model->save();

/** Удаление заявки на возврат
 * @var \Scarlet\Models\AdditionalService $model
 */
$model = new \Scarlet\Models\AdditionalService($client->getHttpClient(), [
    "Ref" => "6cec19e5-cb80-11e6-aaab-005056801329",
]);
$model->delete();

```


### Работа с Изменение данных (API Услуга Изменение данных)

Для работы в этом разделе существует 1 модель: 
- `\Scarlet\Models\AdditionalService`,

```php

/**
 * @var \Scarlet\Models\AdditionalService $additionalService
 */
$additionalService = $client->additionalService;

/** Проверка возможности создания заявки по изменению данных
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\AdditionalService> $collection
 */
$collection = $additionalService->checkPossibilityChangeEW(function (\Scarlet\Core\Build\Builder $builder) {
   $builder->where('IntDocNumber', '20450170534889');
})->get();

/** Получение списка заявок
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\AdditionalService> $collection
 */
$collection = $additionalService->changeEWOrdersList(function (\Scarlet\Core\Build\Builder $builder) {
   $builder->where('BeginDate', '02.02.2015')->where('EndDate', '10.10.2019');
})->get();

/** Создание заявки по изменению данных
 * @var \Scarlet\Models\AdditionalService $model
 */
$model = new \Scarlet\Models\AdditionalService($client->getHttpClient(), [
     "IntDocNumber" => "20600000066986",
     "OrderType" => "orderChangeEW",
     "SenderContactName" => "Пупкін Дмитро",
     "SenderPhone" => "0664561237",
     "Recipient" => "0c38cdbe-84b9-11e6-af9a-005056886752",
     "RecipientContactName" => "Могильна Юлія",
     "RecipientPhone" => "0674071624",
     "PayerType" => "Recipient",
     "PaymentMethod" => "Cash"
 ]);
$model->save();

/** Удаление заявк
 * @var \Scarlet\Models\AdditionalService $model
 */
$model = new \Scarlet\Models\AdditionalService($client->getHttpClient(), [
    "Ref" => "6cec19e5-cb80-11e6-aa33-005056801329",
]);
$model->delete();

```

### Работа с переадресация отправления (API Услуга переадресация отправления)

Для работы в этом разделе существуют 2 модели: 
- `\Scarlet\Models\AdditionalService`,
- `\Scarlet\Models\AdditionalServiceGeneral`.

```php

/**
 * @var \Scarlet\Models\AdditionalService $additionalService
 */
$additionalService = $client->additionalService;

/** Получение списка заявок
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\AdditionalService> $collection
 */
$collection = $additionalService->redirectionOrdersList(function (\Scarlet\Core\Build\Builder $builder) {
   $builder->where('BeginDate', '02.02.2015')->where('EndDate', '10.10.2019')->limit(3);
})->get();

/** Проверка возможности создания заявки на переадресацию отправления
 * @var \Scarlet\Models\AdditionalServiceGeneral $model
 */
$model = $client->additionalServiceGeneral->checkPossibilityForRedirecting(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->where('Number', '20450170534889');
})->get();

/** Создание заявки переадресация отправления (отделение/адрес)
 * @var \Scarlet\Models\AdditionalServiceGeneral $model
 */
$model = new \Scarlet\Models\AdditionalServiceGeneral($client->getHttpClient(), [
     "OrderType" => "orderRedirecting",
     "IntDocNumber" => "20450170534889",
     "Customer" => "Sender",
     "ServiceType" => "DoorsWarehouse",
     "RecipientWarehouse" => "67b2f507-1cb5-11e1-bdca-0024e83b596e",
     "Recipient" => "7c9e1f7a-33cb-11e6-bbb6-005056886752",
     "RecipientContactName" => "Лололо Степан Степанович",
     "RecipientPhone" => "380671234567",
     "PayerType" => "Recipient",
     "PaymentMethod" => "Cash",
     "Note" => "какая-то причина переадресации"
 ]);
$model->save();

/** Удаление заявк
 * @var \Scarlet\Models\AdditionalService $model
 */
$model = new \Scarlet\Models\AdditionalServiceGeneral($client->getHttpClient(), [
    "Ref" => "6cec19e5-cb80-11aa-aa33-005056801329",
]);
$model->delete();

```

### Работа с Экспресс-накладными (API Экспресс-накладная)

Для работы в этом разделе существуют 2 модели: 
- `\Scarlet\Models\InternetDocument`,
- `\Scarlet\Models\TrackingDocument`.

```php

/**
 * @var \Scarlet\Models\InternetDocument $internetDocument
 */
$internetDocument = $client->internetDocument;

/** Получить список ЭН
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\InternetDocument> $collection
 */
// за текущий день
$collection = $internetDocument->get();
// с условием
$collection = $internetDocument->documentList(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->dateTimeFrom('01.10.2019')->dateTimeTo('10.10.2019');
})->get();

/** Расчитать стоимость услуг
 * @var \Scarlet\Models\InternetDocument $model
 */
$model = $internetDocument->documentPrice(function (\Scarlet\Core\Build\Builder $builder) {
     $builder->where('CitySender', '8d5a980d-391c-11dd-90d9-001a92567626')
         ->where('CityRecipient', 'db5c88e0-391c-11dd-90d9-001a92567626')
         ->where('Weight', 3)
         ->where('Cost', 3000)
         ->where('PackCalculate', [
             'PackCount' => 1,
             'PackRef' => '1499fa4a-d26e-11e1-95e4-0026b97ed48a',
         ]);
})->get();
 
/** Прогноз даты доставки груза
 * @var \Scarlet\Models\InternetDocument $model
 */
$model = $internetDocument->documentDeliveryDate(function (\Scarlet\Core\Build\Builder $builder) {
     $builder->where('CitySender', '8d5a980d-391c-11dd-90d9-001a92567626')
         ->where('CityRecipient', 'db5c88e0-391c-11dd-90d9-001a92567626')
         ->where('ServiceType', 'WarehouseDoors');
})->get();
 
/** Формирование запроса для получения полного отчета по накладным
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\InternetDocument> $collection
 */
$collection = $internetDocument->generateReport(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->where('Type', 'xls или csv')->dateTime('09.10.2019')->where('DocumentRefs', [
        '736e0192-eb38-11e9-9937-005056881c6b',
        '7f9ec785-eb38-11e9-9937-005056881c6b',
    ]);
})->get();
 
/** Создать экспресс-накладную
 * Точка входа для создания всех типов ЭН одна в зависимости от переданных параметров
 * будет созданная нужная ЭН, все параметры в офф.док. НП
 * Формирование запросов на создание ЭН с дополнительными услугами и 
 * Формирование запросов на создание ЭН с различными видами груза
 * обрабатываются подобным образом 
 * @var \Scarlet\Models\InternetDocument $model
 */
$model = new \Scarlet\Models\InternetDocument($client->getHttpClient(), [
    "PayerType" => "Sender",
    "PaymentMethod" => "Cash",
    "DateTime" => "10.10.2019",
    "CargoType" => "Cargo",
    "VolumeGeneral" => "0.1",
    "Weight" => "10",
    "ServiceType" => "WarehouseDoors",
    "SeatsAmount" => "1",
    "Description" => "абажур",
    "Cost" => "500",
    "CitySender" => "7d350679-e49e-11e3-8c4a-0050568002cf",
    "Sender" => "29f3b2f7-153c-11e7-8ba8-005056881c6b",
    "SenderAddress" => "90bbdbad-e9a9-11e8-8b24-005056881c6b",
    "ContactSender" => "3fb79e49-cc87-11e8-8b24-005056881c6b",
    "SendersPhone" => "380674627405",
    "CityRecipient" => "f7062316-4078-11de-b509-001d92f78698",
    "Recipient" => "29f7b3a7-153c-11e7-8ba8-005056881c6b",
    "RecipientAddress" => "32abaab9-460d-11e7-8ba8-005056881c6b",
    "ContactRecipient" => "3db7239c-6e15-11e8-8b24-005056881c6b",
    "RecipientsPhone" => "380501234564"
]);
$model->save();

/** Редактировать экспресс-накладную
 * Точка входа для редактирования всех типов ЭН одна в зависимости от переданных параметров
 * будет отредактированная нужная ЭН, все параметры в офф.док. НП
 * @var \Scarlet\Models\InternetDocument $model
 */
$model = $collection->first();
$model->Cost = '550';
$model->update();

/** Удалить экспресс-накладную
 * достаточно передать свойство DocumentRefs в модели
 * @var bool $status
 */
$model = $collection->first();
$status = $model->delete();
 

/** Трекинг
 * @var \Scarlet\Core\Collection\Collection<\Scarlet\Models\TrackingDocument> $collection
 */
$collection = $client->trackingDocument->statusDocuments(function (\Scarlet\Core\Build\Builder $builder) {
    $builder->where('Documents', [
        [
          "DocumentNumber" => "59000451851000",
          "Phone" => "",
        ],
    ]);
})->get();
 
```
