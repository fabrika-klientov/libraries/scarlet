<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Tests
 * @category  Tests
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2019 Fabrika-Klientov
 * @version   GIT: 20.01.31
 * @link      https://fabrika-klientov.ua
 */

namespace Tests;

use Scarlet\Client;
use PHPUnit\Framework\TestCase;
use Scarlet\Core\Query\HttpClient;

class ClientTest extends TestCase
{

    /**
     * @var Client $client
     * */
    protected $client;

    public function test__construct()
    {
        $client = new Client('token');

        $this->assertInstanceOf(Client::class, $client);

    }

    public function testGetHttpClient()
    {
        $this->assertInstanceOf(HttpClient::class, $this->client->getHttpClient());
    }

    protected function setUp(): void
    {
        $this->client = new Client('token');
    }

    protected function tearDown(): void
    {
        $this->client = null;
    }
}
